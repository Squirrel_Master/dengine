#ifndef _MESH_H_
#define _MESH_H_

#include <graphics/texture.h>
#include <graphics/renderbuf3d.h>
#include <graphics/vertex.h>
#include <graphics/renderable.h>
#include <io/io.h>
#include <math/dmath.h>
#include <math/mat.h>

using std::vector;

namespace dengine {

class Mesh : public Renderable3D {
public:
	Mesh() : Renderable3D() {};
	Mesh(const Mesh *m);
	Mesh(const char *filename,
	     const vec4& color,
	     Texture *texture = NULL,
	     const int texture_array_index = -1);
	Mesh(const vector<vec3> *vertices,
	     const vector<unsigned int> *indices,
	     const vec4& color,
	     const vector<vec3> *normals = NULL,
	     const vector<vec3> *tangents = NULL,
	     const vector<vec2> *uv = NULL,
	     Texture *texture = NULL,
	     const int texture_array_index = -1);

	virtual ~Mesh();

	int init(const Mesh *m);
	int init(const char *filename,
		 const vec4& color,
		 Texture *texture = NULL,
		 const int texture_array_index = -1);
	int init(const vector<vec3> *vertices,
		 const vector<unsigned int> *indices,
		 const vec4& color,
		 const vector<vec3> *normals = NULL,
		 const vector<vec3> *tangents = NULL,
		 const vector<vec2> *uv = NULL,
		 Texture *texture = NULL,
		 const int texture_array_index = -1);

	/* Apply a transform to the vertices (cannot be undone). */
	void transform(const mat4 *t);
	void transform(mat4 t);

	void destroy();

	/* Submit the vertex data to the renderbuf. */
	virtual void submit(Renderbuf3D *rbuf, mat4 *t);

protected:
	/* Get the basis vectors (normal, tangent, binormal). */
	void compute_bases();
};

} /* namespace dengine */

#endif /* _MESH_H_ */
