#ifndef _FONT_H_
#define _FONT_H_

#include <string.h>
#include <GL/glew.h>
#include <graphics/texture.h>
#include <math/vec.h>

namespace dengine {

typedef struct char_info {
	float ax, ay;	/* advance.x, advance.y */
	float bw, bh;	/* bitmap.width, bitmap.height */
	float bl, bt;	/* bitmap_left, bitmap_top */
	float tx, ty;	/* x and y offsets in texture coordinates */
} CharInfo;

class Font {
public:
	Font();
	CharInfo c[128];
	Texture *_texture;
	int _idx;
	char *_name;
};

} /* namespace dengine */

#endif /* _FONT_H_ */
