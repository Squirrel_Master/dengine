#ifndef _DUAL_QUAT_H_
#define _DUAL_QUAT_H_

#include <math/vec.h>
#include <math/mat.h>
#include <math/quat.h>

namespace dengine {

class DualQuat {
public:
	DualQuat();
	DualQuat(const Quat& real, const Quat& dual);
	DualQuat(const Quat& rot, const vec3& trans);
	float dot(const DualQuat& d);
	DualQuat operator*(float scalar);
	DualQuat& operator=(const DualQuat& dq);
	DualQuat& normalize();
	DualQuat operator+(const DualQuat& rhs);
	DualQuat operator*(const DualQuat& rhs);
	DualQuat conjugate() const;
	Quat get_rotation() const;
	vec3 get_translation() const;
	mat4 get_matrix();
	void get_matrix(mat4& m);

//private:
	Quat _real, _dual;
};

} /* namespace dengine */

#endif /* _DUAL_QUAT_H_ */
