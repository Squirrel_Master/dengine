#ifndef _MAT_H_
#define _MAT_H_

#include <stdio.h>
#include "vec.h"

namespace dengine {

/* 2D Square Matrix */
class mat2 {

	vec2 _m[2];

	public:
	/* Constructors */
	mat2(const GLfloat d = GLfloat(1.0)) /* Create a diagional matrix */
	{
		_m[0].x = d;
		_m[1].y = d;
	}
	mat2(const vec2 &a, const vec2 &b) { _m[0] = a; _m[1] = b; }
	mat2(GLfloat m00, GLfloat m10, GLfloat m01, GLfloat m11)
	{
		_m[0] = vec2(m00, m01);
		_m[1] = vec2(m10, m11);
	}
	mat2(const mat2 &m)
	{
		if (*this != m) {
			_m[0] = m._m[0];
			_m[1] = m._m[1];
		}
	}

	/* Indexing Operator */
	inline vec2& operator[](int i) { return _m[i]; }
	inline const vec2& operator[](int i) const { return _m[i]; }

	/* Arithmetic Operators (Non-modifying) */
	inline mat2 operator+(const mat2 &m) const
	{
		return mat2(_m[0] + m[0], _m[1] + m[1]);
	}
	inline mat2 operator-(const mat2 &m) const
	{
		return mat2(_m[0] - m[0], _m[1] - m[1]);
	}
	inline mat2 operator*(const GLfloat s) const
	{
		return mat2(s * _m[0], s * _m[1]);
	}
	inline mat2 operator/(const GLfloat s) const
	{
		GLfloat r = GLfloat(1.0) / s;
		return *this * r;
	}
	inline friend mat2 operator*(const GLfloat s, const mat2 &m)
	{
		return m * s;
	}

	mat2 operator*(const mat2 &m) const
	{
		mat2 a(0);
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 2; ++j) {
				for (int k = 0; k < 2; ++k)
					a[i][j] += _m[i][k] * m[k][j];
			}
		}
		return a;
	}

	/* Arithmetic Operators (Modifying) */
	mat2& operator+=(const mat2 &m)
	{
		_m[0] += m[0];
		_m[1] += m[1];
		return *this;
	}
	mat2& operator-=(const mat2 &m)
	{
		_m[0] -= m[0];
		_m[1] -= m[1];
		return *this;
	}
	mat2& operator*=(const GLfloat s)
	{
		_m[0] *= s;
		_m[1] *= s;
		return *this;
	}
	mat2& operator*=(const mat2 &m)
	{
		mat2 a(0);
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 2; ++j) {
				for (int k = 0; k < 2; ++k)
					a[i][j] += _m[i][k] * m[k][j];
			}
		}
		return *this = a;
	}
	mat2& operator/=(const GLfloat s)
	{
		GLfloat r = GLfloat(1.0) / s;
		return *this *= r;
	}

	/* Matrix and Vector operators */
	vec2 operator*(const vec2 &v) const
	{
		return vec2(_m[0][0] * v.x + _m[0][1] * v.y,
			    _m[1][0] * v.x + _m[1][1] * v.y);
	}

	/* Conversion Operators */
	inline operator const GLfloat*(void) const
	{
		return (const GLfloat *)&_m[0].x;
	}
	inline operator GLfloat*(void) { return (GLfloat *)&_m[0].x; }
};

inline mat2 matrixCompMult(const mat2 &A, const mat2 &B)
{
	return mat2(A[0][0] * B[0][0], A[0][1] * B[0][1],
		    A[1][0] * B[1][0], A[1][1] * B[1][1]);
}

inline mat2 transpose(const mat2 &A)
{
	return mat2(A[0][0], A[1][0],
		    A[0][1], A[1][1]);
}

/* 3D Square Matrix */
class mat3 {

	vec3 _m[3];

	public:

	/* Constructors */
	mat3(const GLfloat d = GLfloat(1.0)) /* Create diagonal matrix */
	{
		_m[0].x = d;
		_m[1].y = d;
		_m[2].z = d;
	}
	mat3(const vec3 &a, const vec3 &b, const vec3 &c)
	{
		_m[0] = a;
		_m[1] = b;
		_m[2] = c;
	}
	mat3(GLfloat m00, GLfloat m10, GLfloat m20,
	     GLfloat m01, GLfloat m11, GLfloat m21,
	     GLfloat m02, GLfloat m12, GLfloat m22)
	{
		_m[0] = vec3(m00, m01, m02);
		_m[1] = vec3(m10, m11, m12);
		_m[2] = vec3(m20, m21, m22);
	}
	mat3(const mat3 &m)
	{
		if (*this != m) {
			_m[0] = m._m[0];
			_m[1] = m._m[1];
			_m[2] = m._m[2];
		}
	}

	/* Indexing Operator */
	inline vec3& operator[](int i) { return _m[i]; }
	inline const vec3& operator[](int i) const { return _m[i]; }

	/* Arithmetic Operators (Non-modifying) */
	inline mat3 operator+(const mat3 &m) const
	{
		return mat3(_m[0] + m[0], _m[1] + m[1], _m[2] + m[2]);
	}
	inline mat3 operator-(const mat3 &m) const
	{
		return mat3(_m[0] - m[0], _m[1] - m[1], _m[2] - m[2]);
	}
	inline mat3 operator*(const GLfloat s) const
	{
		return mat3(s * _m[0], s * _m[1], s * _m[2]);
	}

	mat3 operator/(const GLfloat s) const
	{
		GLfloat r = GLfloat(1.0)/s;
		return *this*r;
	}

	inline friend mat3 operator*(const GLfloat s, const mat3 &m)
	{
		return m * s;
	}
	mat3 operator*(const mat3 &m) const
	{
		mat3 a(0);
		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 3; ++j) {
				for (int k = 0; k < 3; ++k)
					a[i][j] += _m[i][k] * m[k][j];
			}
		}
		return a;
	}

	/* Arithmetic Operators (Modifying) */
	mat3& operator+=(const mat3 &m)
	{
		_m[0] += m[0];
		_m[1] += m[1];
		_m[2] += m[2];
		return *this;
	}
	mat3& operator-=(const mat3 &m)
	{
		_m[0] -= m[0];
		_m[1] -= m[1];
		_m[2] -= m[2];
		return *this;
	}
	mat3& operator*=(const GLfloat s)
	{
		_m[0] *= s;
		_m[1] *= s;
		_m[2] *= s;
		return *this;
	}
	mat3& operator*=(const mat3 &m)
	{
		mat3 a(0);
		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 3; ++j) {
				for (int k = 0; k < 3; ++k)
					a[i][j] += _m[i][k] * m[k][j];
			}
		}
		return *this = a;
	}
	mat3& operator/=(const GLfloat s)
	{
		GLfloat r = GLfloat(1.0)/s;
		return *this *= r;
	}

	/* Matrix and Vector operators */
	vec3 operator*(const vec3 &v) const
	{
		return vec3(_m[0][0] * v.x + _m[0][1] * v.y + _m[0][2] * v.z,
			    _m[1][0] * v.x + _m[1][1] * v.y + _m[1][2] * v.z,
			    _m[2][0] * v.x + _m[2][1] * v.y + _m[2][2] * v.z);
	}

	/* Conversion Operators */
	inline operator const GLfloat* (void) const
	{
		return (const GLfloat *)&_m[0].x;
	}
	inline operator GLfloat* (void) { return (GLfloat *)&_m[0].x; }
};

inline mat3 matrixCompMult(const mat3 &A, const mat3 &B)
{
	return mat3(A[0][0] * B[0][0], A[0][1] * B[0][1], A[0][2] * B[0][2],
		    A[1][0] * B[1][0], A[1][1] * B[1][1], A[1][2] * B[1][2],
		    A[2][0] * B[2][0], A[2][1] * B[2][1], A[2][2] * B[2][2]);
}

inline mat3 transpose(const mat3 &A)
{
	return mat3(A[0][0], A[1][0], A[2][0],
		    A[0][1], A[1][1], A[2][1],
		    A[0][2], A[1][2], A[2][2]);
}

/* 4D Square Matrix */
class mat4 {

	vec4  _m[4];

	public:
	/* Constructors */
	mat4(const GLfloat d = GLfloat(1.0)) /* Create diagonal matrix */
	{
		_m[0].x = d;
		_m[1].y = d;
		_m[2].z = d;
		_m[3].w = d;
	}
	mat4(const vec4 &a, const vec4 &b, const vec4 &c, const vec4 &d)
	{
		_m[0] = a;
		_m[1] = b;
		_m[2] = c;
		_m[3] = d;
	}
	mat4(GLfloat m00, GLfloat m10, GLfloat m20, GLfloat m30,
	     GLfloat m01, GLfloat m11, GLfloat m21, GLfloat m31,
	     GLfloat m02, GLfloat m12, GLfloat m22, GLfloat m32,
	     GLfloat m03, GLfloat m13, GLfloat m23, GLfloat m33)
	{
		_m[0] = vec4(m00, m01, m02, m03);
		_m[1] = vec4(m10, m11, m12, m13);
		_m[2] = vec4(m20, m21, m22, m23);
		_m[3] = vec4(m30, m31, m32, m33);
	}
	mat4(const mat4 &m)
	{
		if (*this != m) {
			_m[0] = m._m[0];
			_m[1] = m._m[1];
			_m[2] = m._m[2];
			_m[3] = m._m[3];
		}
	}

	/* Indexing Operator */
	inline vec4& operator[](int i) { return _m[i]; }
	inline const vec4& operator[](int i) const { return _m[i]; }

	/* Arithmetic Operators (Non-modifying) */
	inline mat4 operator+(const mat4 &m) const
	{
		return mat4(_m[0] + m[0],
			    _m[1] + m[1],
			    _m[2] + m[2],
			    _m[3] + m[3]);
	}
	inline mat4 operator-(const mat4 &m) const
	{
		return mat4(_m[0] - m[0],
			    _m[1] - m[1],
			    _m[2] - m[2],
			    _m[3] - m[3]);
	}
	inline mat4 operator*(const GLfloat s) const
	{
		return mat4(s * _m[0], s * _m[1], s * _m[2], s * _m[3]);
	}
	mat4 operator/(const GLfloat s) const
	{
		GLfloat r = GLfloat(1.0)/s;
		return *this*r;
	}
	inline friend mat4 operator*(const GLfloat s, const mat4 &m)
	{
		return m * s;
	}
	mat4 operator*(const mat4 &m) const
	{
		mat4 a(0);
		for (int i = 0; i < 4; ++i) {
			for (int j = 0; j < 4; ++j) {
				for (int k = 0; k < 4; ++k)
					a[i][j] += _m[i][k] * m[k][j];
			}
		}

		return a;
	}

	/* Arithmetic Operators (Modifying) */
	mat4& operator+=(const mat4 &m)
	{
		_m[0] += m[0];
		_m[1] += m[1];
		_m[2] += m[2];
		_m[3] += m[3];
		return *this;
	}
	mat4& operator-=(const mat4 &m)
	{
		_m[0] -= m[0];
		_m[1] -= m[1];
		_m[2] -= m[2];
		_m[3] -= m[3];
		return *this;
	}
	mat4& operator*=(const GLfloat s)
	{
		_m[0] *= s;
		_m[1] *= s;
		_m[2] *= s;
		_m[3] *= s;
		return *this;
	}
	mat4& operator*=(const mat4 &m)
	{
		mat4 a(0);
		for (int i = 0; i < 4; ++i) {
			for (int j = 0; j < 4; ++j) {
				for (int k = 0; k < 4; ++k)
					a[i][j] += _m[i][k] * m[k][j];
			}
		}
		return *this = a;
	}
	mat4& operator/=(const GLfloat s)
	{
		GLfloat r = GLfloat(1.0)/s;
		return *this *= r;
	}

	/* Matrix and Vector Operators */
	inline vec4 operator*(const vec4 &v) const
	{
		return vec4(
		_m[0][0] * v.x + _m[0][1] * v.y + _m[0][2] * v.z + _m[0][3] * v.w,
		_m[1][0] * v.x + _m[1][1] * v.y + _m[1][2] * v.z + _m[1][3] * v.w,
		_m[2][0] * v.x + _m[2][1] * v.y + _m[2][2] * v.z + _m[2][3] * v.w,
		_m[3][0] * v.x + _m[3][1] * v.y + _m[3][2] * v.z + _m[3][3] * v.w);
	}

	inline vec3 operator*(const vec3 &v) const
	{
		return vec3(
		_m[0][0] * v.x + _m[0][1] * v.y + _m[0][2] * v.z + _m[0][3],
		_m[1][0] * v.x + _m[1][1] * v.y + _m[1][2] * v.z + _m[1][3],
		_m[2][0] * v.x + _m[2][1] * v.y + _m[2][2] * v.z + _m[2][3]);
	}

	/* Conversion Operators */
	inline operator const GLfloat*() const
	{
		return (const GLfloat *)&_m[0].x;
	}
	inline operator GLfloat*() { return (GLfloat *)&_m[0].x; }
};

/* Matrix component multiplication */
inline mat4 matrix_comp_mult(const mat4 &A, const mat4 &B)
{
	return mat4(
	A[0][0] * B[0][0], A[0][1] * B[0][1], A[0][2] * B[0][2], A[0][3] * B[0][3],
	A[1][0] * B[1][0], A[1][1] * B[1][1], A[1][2] * B[1][2], A[1][3] * B[1][3],
	A[2][0] * B[2][0], A[2][1] * B[2][1], A[2][2] * B[2][2], A[2][3] * B[2][3],
	A[3][0] * B[3][0], A[3][1] * B[3][1], A[3][2] * B[3][2], A[3][3] * B[3][3]);
}

inline mat4 transpose(const mat4 &A)
{
	return mat4(A[0][0], A[1][0], A[2][0], A[3][0],
		    A[0][1], A[1][1], A[2][1], A[3][1],
		    A[0][2], A[1][2], A[2][2], A[3][2],
		    A[0][3], A[1][3], A[2][3], A[3][3]);
}

/* Rotations */
inline mat4 rotate_x(const GLfloat theta)
{
	GLfloat angle = deg_to_rad * theta;
	mat4 m;
	m[2][2] = m[1][1] = cos(angle);
	m[2][1] = sin(angle);
	m[1][2] = -m[2][1];
	return m;
}

inline mat4 rotate_y(const GLfloat theta)
{
	GLfloat angle = deg_to_rad * theta;
	mat4 c;
	c[2][2] = c[0][0] = cos(angle);
	c[0][2] = sin(angle);
	c[2][0] = -c[0][2];
	return c;
}

inline mat4 rotate_z(const GLfloat theta)
{
	GLfloat angle = deg_to_rad * theta;
	mat4 c;
	c[0][0] = c[1][1] = cos(angle);
	c[1][0] = sin(angle);
	c[0][1] = -c[1][0];
	return c;
}

inline mat4 rotation(const GLfloat angle, const vec3 &axis)
{
	mat4 ret;
	GLfloat a = angle * deg_to_rad;
	GLfloat c = cos(a);
	GLfloat s = sin(a);
	GLfloat omc = 1.f - c;

	GLfloat x = axis.x;
	GLfloat y = axis.y;
	GLfloat z = axis.z;

	ret[0][0] = x * omc + c;
	ret[1][0] = x * y * omc + z * s;
	ret[2][0] = x * z * omc - y * s;

	ret[0][1] = x * y * omc - z * s;
	ret[1][1] = y * omc + c;
	ret[2][1] = y * z * omc + x * s;

	ret[0][2] = x * z * omc + y * s;
	ret[1][2] = y * z * omc - x * s;
	ret[2][2] = z * omc + c;

	return ret;
}

/* Translations */
inline mat4 translation(const GLfloat x, const GLfloat y, const GLfloat z)
{
	mat4 c(1.f);
	c[0][3] = x;
	c[1][3] = y;
	c[2][3] = z;

	return c;
}

inline mat4 translation(const vec3 &v)
{
	return translation(v.x, v.y, v.z);
}

inline mat4 translation(const vec4 &v)
{
	return translation(v.x, v.y, v.z);
}

/* Scaling */
inline mat4 scale(const GLfloat x, const GLfloat y, const GLfloat z)
{
	mat4 m;
	m[0][0] = x;
	m[1][1] = y;
	m[2][2] = z;
	return m;
}

inline mat4 scale(const vec3 &v)
{
	return scale(v.x, v.y, v.z);
}

/*
 * Projection transformations
 *
 * Note: Microsoft Windows (r) defines the keyword "far" in C/C++.  In
 * order to avoid name conflicts, we append 'z' to variable names. NBD.
 */
inline mat4 ortho(const GLfloat left,
		  const GLfloat right,
		  const GLfloat bottom,
		  const GLfloat top,
		  const GLfloat zNear,
		  const GLfloat zFar)
{
	mat4 c(1.f);
	c[0][0] = 2.f / (right - left);
	c[1][1] = 2.f / (top - bottom);
	c[2][2] = 2.f / (zNear - zFar);
	c[3][0] = (right + left) / (left - right);
	c[3][1] = (top + bottom) / (bottom - top);
	c[3][2] = (zFar + zNear) / (zNear - zFar);

	return c;
}

inline mat4 ortho2(const GLfloat left,
		   const GLfloat right,
		   const GLfloat bottom,
		   const GLfloat top)
{
	return ortho(left, right, bottom, top, -1.0, 1.0);
}

inline mat4 frustum(const GLfloat left,
		    const GLfloat right,
		    const GLfloat bottom,
		    const GLfloat top,
		    const GLfloat zNear,
		    const GLfloat zFar)
{
	mat4 m;

	GLfloat range = GLfloat(1.0) / (right - left);
	m[0][0] = 2.f * zNear * range;
	m[0][2] = (right + left) * range;

	range = GLfloat(1.0) / (top - bottom);
	m[1][1] = 2.f * zNear * range;
	m[1][2] = (top + bottom) * range;

	range = GLfloat(1.0) / (zNear - zFar);
	m[2][2] = (zFar + zNear) * range;
	m[2][3] = 2.0 * zFar * zNear * range;
	m[3][2] = -1.0;

	return m;
}

inline mat4 perspective(const GLfloat fovy,
			const GLfloat aspect,
			const GLfloat zNear,
			const GLfloat zFar)
{
	GLfloat top = tan(fovy * deg_to_rad * .5f) * zNear;
	GLfloat right = top * aspect;
	GLfloat zRange = GLfloat(1.0) / (zNear - zFar);
	mat4 m;
	m[0][0] = zNear / right;
	m[1][1] = zNear / top;
	m[2][2] = (zFar + zNear) * zRange;
	m[2][3] = 2.0f * zFar * zNear * zRange;
	m[3][2] = -1.0f;

	return m;
}

inline void printv(const vec4& a)
{
	printf("%f %f %f %f\n", a[0], a[1], a[2], a[3]);
}

/* Viewing Transformation */
inline mat4 look_at(const vec4& eye, const vec4& unit_dir, const vec4& up)
{
	vec4 right = vec4(normalize(cross(unit_dir, up)), 0.f);
	vec4 new_up = vec4(normalize(cross(right, unit_dir)), 0.f);
	return mat4(right,
		    new_up,
		    -unit_dir,
		    vec4(0.f, 0.f, 0.f, 1.f)) * translation(-eye);
}


inline void printm(const mat4& a)
{

	for(int i = 0; i < 4; ++i)
		printf("%f %f %f %f\n", a[i][0], a[i][1], a[i][2], a[i][3]);
}
inline void printm(const mat3& a)
{

	for(int i = 0; i < 3; ++i)
		printf("%f %f %f\n", a[i][0], a[i][1], a[i][2]);
}

inline mat4 identity()
{
	mat4 m;
	return m;
}

} /* namespace dengine */

#endif /* _MAT_H_ */
