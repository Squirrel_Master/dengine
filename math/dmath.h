#ifndef _DMATH_H_
#define _DMATH_H_

#include <math.h>
#include <GL/glew.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define BUFFER_OFFSET(st, m) ((GLvoid *)offsetof(st, m))

static const GLfloat deg_to_rad = M_PI / 180.f;

#endif /* _DMATH_H_ */
