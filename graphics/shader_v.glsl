#version 130

in vec4 position; /* layout location = 0 */
in vec4 normal;   /* layout location = 1 */
in vec4 tangent;  /* layout location = 2 */
in vec2 uv;	  /* layout location = 3 */
in float t_id;    /* layout location = 4 */
in float t_index; /* layout location = 5 */
in vec4 color;    /* layout location = 6 */

out vec4 v_pos;
out vec4 v_normal;
out vec4 v_tangent;
out vec2 v_uv;
out float v_t_id;
out float v_t_index;
out vec4 v_color;

//uniform mat4 proj_matrix;
//uniform mat4 view_matrix = mat4(1.0);
uniform mat4 VP_matrix; // view and projection matrix
uniform mat4 model_matrix = mat4(1.0);

void main()
{
	v_pos = model_matrix * position;
	v_color = color;
	v_uv = uv;
	v_t_id = t_id;
	v_t_index = t_index;

//	v_normal = model_matrix * normal;

	// want w component to be 0 because we want rotation and scale, but
        // not translation.
	v_normal = normalize(model_matrix * vec4(normal.xyz, 0.f));
	v_tangent = tangent;

	gl_Position = VP_matrix * v_pos;
}
