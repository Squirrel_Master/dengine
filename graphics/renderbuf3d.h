#ifndef _RENDERBUF3D_H_
#define _RENDERBUF3D_H_

#include <math/dmath.h>
#include <math/vec.h>
#include <graphics/vertex.h>
#include <vector>
#include <unordered_set>
#include <graphics/texture.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

namespace dengine {

#define MVERTEX_SIZE sizeof(MeshVertex)

/* Position of color and pos in struct vertex */
#define MPOSITION_OFFSET BUFFER_OFFSET(MeshVertex, pos)
#define MNORMAL_OFFSET   BUFFER_OFFSET(MeshVertex, normal)
#define MTANGENT_OFFSET  BUFFER_OFFSET(MeshVertex, tangent)
#define MUV_OFFSET       BUFFER_OFFSET(MeshVertex, uv)
#define MTID_OFFSET      BUFFER_OFFSET(MeshVertex, t_id)
#define MTIDX_OFFSET     BUFFER_OFFSET(MeshVertex, t_idx)
#define MCOLOR_OFFSET    BUFFER_OFFSET(MeshVertex, color)

/* Layout position in the shader */
#define MPOSITION_INDEX 0
#define MNORMAL_INDEX   1
#define MTANGENT_INDEX  2
#define MUV_INDEX       3
#define MTID_INDEX      4
#define MTIDX_INDEX     5
#define MCOLOR_INDEX    6

using std::vector;
using std::unordered_set;
typedef unordered_set<Texture *>::iterator set_iter;

/*
 * Renderbuf3D:
 */
class Renderbuf3D {
public:
	Renderbuf3D();
	~Renderbuf3D();

	/* init(): initializes the Renderbuf3D. */
	void init();
	void init(unsigned int vbo_size, unsigned int ibo_size);
	void init_vbo(unsigned int vbo_size);
	void init_ibo(unsigned int ibo_size);

	/* destroy(): deletes all allocated variables. */
	void destroy();

	/* Call at the very beginning of any rendering routine. */
	void reset() { _draw_calls = 0; }

	void begin();
	void end();
	void flush();
	void insert_texture(Texture *t);

	/* glMapBuffers: _buf for the vbo, _buf_idx for the ibo. */
	MeshVertex *_buf;
	GLuint *_buf_idx;

	GLuint _vao, _vbo, _ibo;

	/* Maintains set of texture slots in shader (constant lookup). */
	unordered_set<Texture *> _tslots;

	int _draw_calls;

	unsigned int _num_vertices, _num_indices;
	unsigned int _sub_vertices, _sub_indices;
};

}

#endif /* _RENDERBUF3D_H_ */

