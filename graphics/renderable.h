#ifndef _RENDERABLE_H_
#define _RENDERABLE_H_

#include <math/mat.h>
#include <graphics/renderbuf2d.h>
#include <graphics/renderbuf3d.h>
#include <graphics/vertex.h>

namespace dengine {

class Renderable {
public:
	Renderable() : _color(0) {};
	virtual ~Renderable() {};

	/*
	 * Genericized so that Renderer3D could submit Renderable2Ds,
	 * thus acting as a general purpose renderer. Currently,
	 * Renderer2D cannot deal with Renderable3Ds.
	 */
	virtual void submit(Renderbuf2D *rbuf, mat4 *t) {};
	virtual void submit(Renderbuf3D *rbuf, mat4 *t) = 0;

	inline void set_color(float r, float g, float b, float a)
	{
		_color = pack_color(r, g, b, a);
	}

	inline const unsigned int get_color() const { return _color; }

protected:
	unsigned int _color;
};

class Renderable2D : public Renderable {
public:
	Renderable2D() : _pos(0.f, 0.f, 0.f) {};
	virtual ~Renderable2D() {};
	virtual void submit(Renderbuf2D *rbuf, mat4 *t) = 0;
	virtual void submit(Renderbuf3D *rbuf, mat4 *t) = 0;

	inline void set_pos(const vec3& v) { _pos = v; }
	inline const vec3& get_pos() const { return _pos; }

protected:
	vec3 _pos;
};

class Renderable3D : public Renderable {
public:
	Renderable3D() : _vertices(NULL),
			 _normals(NULL),
			 _tangents(NULL),
			 _uv(NULL),
			 _indices(NULL),
			 _texture(NULL),
			 _num_vertices(0),
			 _num_indices(0),
			 _idx(-1.f) {};
	virtual ~Renderable3D() {};
	virtual void submit(Renderbuf3D *rbuf, mat4 *t) = 0;

protected:
	unsigned int _num_vertices, _num_indices;
	vec3 *_vertices, *_normals, *_tangents;
	vec2 *_uv;
	Texture *_texture;
	float _idx;
	unsigned int *_indices;
};

} /* namespace dengine */

#endif /* _RENDERABLE_H_ */
