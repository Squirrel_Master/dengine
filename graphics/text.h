#ifndef _TEXT_H_
#define _TEXT_H_

#include <graphics/texture.h>
#include <graphics/font.h>
#include <graphics/font_manager.h>
#include <graphics/renderbuf2d.h>
#include <math/mat.h>
#include <graphics/renderable.h>

namespace dengine {

/*==== COORDINATE LAYOUT ===|

  (x0,y0) 0-------3 (x1,y0)
          |\      |
          |  \    |
          |    \  |
          |      \|
  (x0,y1) 1-------2 (x1,y1)

Indices: [0,1,2,2,3,0]

NOTE: Differs from Sprite because of
FreeType's goofy font format.
|====================================*/

class Text : public Renderable2D {
public:
	Text();
	Text(const char *str,
	     const Font *font,
	     const vec3& position,
	     const vec2& scale,
	     const vec2& aspect,
	     const vec4& color);
	virtual ~Text();

	void destroy();

	int set_text(const char *str,
		     const Font *font,
		     const vec3& position,
		     const vec2& scale,
		     const vec2& aspect,
		     const vec4& color);

	int set_text(const char *str, const Font *font = NULL);
	int append_text(const char *str);

	void set_aspect(float x, float y);
	void set_scale(float x, float y);

	virtual void submit(Renderbuf2D *rbuf, mat4 *t);
	virtual void submit(Renderbuf3D *rbuf, mat4 *t);

	inline const Font *get_font() const { return _font; }
	inline void set_font(Font *f) { if (f) _font = f; }
	inline const char *get_str() const { return _str; }

private:
	const Font *_font;
	char *_str;
	vec2 _scale;	/* Maintain scale in relation to font's size. */
	vec2 _aspect;
};

}

#endif /* _TEXT_H_ */
