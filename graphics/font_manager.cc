#include <graphics/font_manager.h>

namespace dengine {

FontManager::FontManager() : _ftlib(NULL)
{
	init_freetype();
}

FontManager::~FontManager()
{
	destroy();
}

void FontManager::destroy()
{
	/*
	 * Clear the vector of TextureArray pointers.
	 * NOTE: The renderer deletes the textures.
	 */
	_textures.clear();

	/* Free all font names. */
	for (int i = 0; i < _fonts.size(); i++)
		free(_fonts[i]._name);

	_fonts.clear();

	/* Free FreeType resources. */
	for (int i = 0; i < _faces.size(); i++)
		FT_Done_Face(_faces[i]);

	_faces.clear();

	if (_ftlib) {
		FT_Done_FreeType(_ftlib);
		_ftlib = NULL;
	}
}

/*
 * add_font:
 * Add a font and its 0th face to _fonts and _faces, respectively.
 */
int FontManager::add_font(const char *fontname,
			  const char *filename,
			  int pixel_height)
{
	if (!fontname || !filename || pixel_height < 1)
		return -1;

	init_freetype();

	/* Check if the font has already been added. */
	if (get_font(fontname))
		return 0;

	FT_Face face;
	if (FT_New_Face(_ftlib, filename, 0, &face)) {
		fprintf(stderr, "Could not open font %s at %s.\n", fontname,
								filename);
		return -1;
	}

	/*
	 * Setting the pixel sizes here determines the clarity in scaling
	 * later, thus lower values result in lower resolutions.
	 */
	FT_Set_Pixel_Sizes(face, 0, pixel_height);

	Font font;
	int len = strlen(fontname);
	font._name = (char *)malloc(len + 1);
	if (!font._name) {
		fprintf(stderr, "malloc failed.\n");
		return -1;
	}

	font._name[len] = '\0';
	strcpy(font._name, fontname);

	_fonts.push_back(font);
	_faces.push_back(face);

	return 0;
}

Font *FontManager::get_font(const char *fontname)
{
	if (!fontname)
		return NULL;

	for (int i = 0; i < _fonts.size(); i++) {
		if (!strcmp(fontname, _fonts[i]._name))
			return &_fonts[i];
	}

	return NULL;
}

int FontManager::init_freetype()
{
	/* Initialize FreeType. */
	if (!_ftlib && FT_Init_FreeType(&_ftlib)) {
		fprintf(stderr, "Failed to initialize FreeType.\n");
		return -1;
	}

	return 0;
}

/*
 * build_texture_array:
 * Builds the texture array given the current fonts contained in _fonts and
 * _faces. After the Texture2D_Array is created, all FreeType resources are
 * released as they are no longer needed. However, if a new font is needed
 * later and a corresponding sampler2DArray has been added to the shader,
 * simply add the desired font to the fontmanager (via add_font) and build
 * the texture (via build_texture_array) with the new sampler2DArray's
 * layout position (or offset).
 *
 * Note: Bear in mind, each additional sampler2DArray in the shader decreases
 * performance, therefore its best to pack all fonts into one Texture2D_Array.
 * It is best to add relatively high resolution fonts (high pixel height) to
 * the fontmanager once and merely scale the text later, as needed, as opposed
 * to maintaining multiple sets of Texture2D_Arrays for different font sizes.
 */
int FontManager::build_texture_array(int layout_offset)
{
	_textures.push_back(new TextureArray(_fonts, _faces, layout_offset));

	/* Destroy Freetype resources as they are no longer needed. */
	for (int i = 0; i < _faces.size(); i++)
		FT_Done_Face(_faces[i]);

	_faces.clear();

	FT_Done_FreeType(_ftlib);
	_ftlib = NULL;

	return 0;
}

} /* namespace dengine */
