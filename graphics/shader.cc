#include <graphics/shader.h>

namespace dengine {

Shader::~Shader()
{
	destroy();
}

/*
 * destroy:
 * Delete the compiled vertex and fragment shaders, if they were not linked,
 * and then delete the shader program (_pid).
 */
void Shader::destroy()
{
	if (_fid) {
		glDeleteShader(_fid);
		_fid = 0;
	}
	if (_vid) {
		glDeleteShader(_vid);
		_vid = 0;
	}
	if (_pid) {
		glDeleteProgram(_pid);
		_pid = 0;
	}

	_uniform_map.clear();
}

/*
 * init:
 * Initializes the shader by creating an empty program object (_pid), compiling
 * the vertex and fragment shaders (_vid and _fid, respectively).
 */
int Shader::init(const char *file_v, const char *file_f)
{
	if (!(_pid = glCreateProgram())) {
		fprintf(stderr, "shader.cc: glCreateProgram() error.\n");
		destroy();
		return -1;
	}

	if (!(_vid = glCreateShader(GL_VERTEX_SHADER))) {
		fprintf(stderr, "shader.cc: glCreateShader(vertex) error.\n");
		destroy();
		return -1;
	}

	if (!(_fid = glCreateShader(GL_FRAGMENT_SHADER))) {
		fprintf(stderr, "shader.cc: glCreateShader(fragment) error.\n");
		destroy();
		return -1;
	}

	if (compile(file_v, _vid) < 0) {
		destroy();
		return -1;
	}

	return compile(file_f, _fid);
}

/*
 * compile:
 * Compiles a shader (referenced by its id resulting from glCreateShader()).
 */
int Shader::compile(const char *filename, GLuint id)
{
	GLint gl_tmp = 0;

	char *source = read_file(filename);
	if (!source) {
		fprintf(stderr, "Unable to read %s\n", filename);
		return -1;
	}

	glShaderSource(id, 1, (const GLchar **)&source, NULL);

	glCompileShader(id);

	glGetShaderiv(id, GL_COMPILE_STATUS, &gl_tmp);

	if (!gl_tmp) {
		char *msg;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &gl_tmp);
		msg = new char[gl_tmp];
		glGetShaderInfoLog(id, gl_tmp, NULL, msg);
		fprintf(stderr, "shader.cc: glCompileShader() error\n%s\n",
									msg);
		delete[] msg;

		return -1;
	}

	if (source)
		delete[] source;

	return 0;
}

/*
 * link:
 * Attaches the vertex and fragment shaders to the program specified by _pid.
 * Afterwards, glLinkProgram creates an executable to run on the programmable
 * vertex/geometry/fragment processors (NOTE: currently only using vertex and
 * fragment shaders).  Following successful linkage, the shader objects may
 * be (and are) detached and deleted.
 */
int Shader::link()
{
	GLint gl_tmp = 0;

	glAttachShader(_pid, _vid);
	glAttachShader(_pid, _fid);

	glLinkProgram(_pid);
	glGetProgramiv(_pid, GL_LINK_STATUS, (int *)&gl_tmp);

	 if (!gl_tmp) {
		char *msg;
		glGetProgramiv(_pid, GL_INFO_LOG_LENGTH, &gl_tmp);
		msg = new char[gl_tmp];
		glGetProgramInfoLog(_pid, gl_tmp, NULL, msg);
		fprintf(stderr, "shader.cc:\n%s\n", msg);

		delete[] msg;

		destroy();

		return -1;
	}

	glDetachShader(_pid, _vid);
	glDetachShader(_pid, _fid);
	glDeleteShader(_vid);
	glDeleteShader(_fid);

	_vid = _fid = 0;

	return 0;
}

/*
 * add_attribute:
 * Adds an attribute by name and current offset, indicated by the attribute
 * counter _attr, to the shader program object.
 */
void Shader::add_attribute(const char *name)
{
	glBindAttribLocation(_pid, _attr++, name);
}

GLint Shader::set_uniform_1f(const char *name, const float val)
{
	GLint loc = get_uniform_loc(name);
	if (loc >= 0)
		glUniform1f(loc, val);

	return loc;
}

GLint Shader::set_uniform_1i(const char *name, const int val)
{
	GLint loc = get_uniform_loc(name);
	if (loc >= 0)
		glUniform1i(loc, val);
	return loc;
}

GLint Shader::set_uniform_2f(const char *name, const vec2 &v)
{
	GLint loc = get_uniform_loc(name);
	if (loc >= 0)
		glUniform2f(loc, v.x, v.y);
	return loc;
}

GLint Shader::set_uniform_3f(const char *name, const vec3 &v)
{
	GLint loc = get_uniform_loc(name);
	if (loc >= 0)
		glUniform3f(loc, v.x, v.y, v.z);
	return loc;
}

GLint Shader::set_uniform_1iv(const char *name, const int count, GLuint *t)
{
	GLint loc = get_uniform_loc(name);
	if (loc >= 0)
		glUniform1iv(loc, count, (const GLint *)t);
	return loc;
}

GLint Shader::set_uniform_4f(const char *name, const vec4 &v)
{
	GLint loc = get_uniform_loc(name);
	if (loc >= 0)
		glUniform4f(loc, v.x, v.y, v.z, v.w);
	return loc;
}

GLint Shader::set_uniform_mat4(const char *name, const mat4 &m, GLboolean transpose)
{
	GLint loc = get_uniform_loc(name);
	if (loc >= 0)
		glUniformMatrix4fv(loc, 1, transpose, m);
	return loc;
}

void Shader::set_uniform_1f(GLint loc, const float val)
{
	glUniform1f(loc, val);
}

void Shader::set_uniform_1i(GLint loc, const int val)
{
	glUniform1i(loc, val);
}

void Shader::set_uniform_2f(GLint loc, const vec2 &v)
{
	glUniform2f(loc, v.x, v.y);
}

void Shader::set_uniform_3f(GLint loc, const vec3 &v)
{
	glUniform3f(loc, v.x, v.y, v.z);
}

void Shader::set_uniform_1iv(GLint loc, const int count, GLuint *t)
{
	glUniform1iv(loc, count, (const GLint *)t);
}

void Shader::set_uniform_4f(GLint loc, const vec4 &v)
{
	glUniform4f(loc, v.x, v.y, v.z, v.w);
}

void Shader::set_uniform_mat4(GLint loc, const mat4 &m, GLboolean transpose)
{
	glUniformMatrix4fv(loc, 1, transpose, m);
}

unsigned long get_hash_code(const char *str)
{
	unsigned long hash = 5381;
	for (int i = 0; i < strlen(str); i++)
		hash = ((hash << 5) + hash) + (int)str[i];
	return hash;
}

GLint Shader::get_uniform_loc(const char *uniform_name)
{
	GLint loc;

	/* Create a valid hash from the uniform's name. */
	unsigned long hash_code = get_hash_code(uniform_name);

	/* Check whether the uniform location has already been inserted. */
	map_iter it = _uniform_map.find(hash_code);

	/*
	 * The uniform location has not been inserted, thus get the location
	 * and add it to uniform_map.
	 *
	 * NOTE: Even if the uniform is not found in the shader, insert the
	 * invalid location in order to avoid searching in vain each time.
	 */
	if (it == _uniform_map.end()) {
		if ((loc = glGetUniformLocation(_pid, uniform_name)) < 0)
			fprintf(stderr, "Unable to find uniform %s.\n",
								uniform_name);

		_uniform_map.insert({hash_code, loc});
	} else {
		loc = it->second;
	}

	return loc;
}

void Shader::enable()
{
	if (!_enabled) {
		glUseProgram(_pid);
		for (int i = 0; i < _attr; ++i)
			glEnableVertexAttribArray(i);

		_enabled = 1;
	}
}

void Shader::disable()
{
	if (_enabled) {
		glUseProgram(0);
		for (int i = 0; i < _attr; ++i)
			glDisableVertexAttribArray(i);
		_enabled = 0;
	}
}

} /* namespace dengine */
