#ifndef _RENDERER3D_H_
#define _RENDERER3D_H_

#include <vector>

#include <math/dmath.h>
#include <graphics/renderer.h>
#include <graphics/renderbuf3d.h>
#include <graphics/batch.h>
#include <graphics/renderable.h>

namespace dengine {

/*
 * class Renderer3d:
 * Renderer3d implements Renderer with the intention of rendering all
 * Renderables, but optimized for Renderable3Ds.
 */
class Renderer3D : public Renderer<Renderable> {
public:
        Renderer3D();
        ~Renderer3D();

	/* init(): initializes the renderer. */
        void init();

	/* destroy(): deletes all allocated variables. */
        void destroy();

	void add(Batch<Renderable> *parent,
					Batch<Renderable> *child = NULL);
	void remove(Batch<Renderable> *batch);
        void render();

	void transform(mat4 m);
	void transform(const mat4 *m);

protected:
	void submit(Batch<Renderable> *batch);

private:
	Renderbuf3D *_rbuf;
	int _draw_calls;
	vector<mat4> _transform_stack;
};

} /* namespace dengine */

#endif /* _RENDERER2D_H_ */
