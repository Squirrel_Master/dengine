#version 130

in vec4 v_pos;
in vec4 v_normal;
in vec4 v_tangent;
in vec2 v_uv;
in float v_t_id;
in float v_t_index;
in vec4 v_color;

out vec4 frag_color;

uniform vec4 color;
uniform vec4 light_pos;
uniform vec4 light_ambience;
uniform vec4 light_diffuse;
uniform vec4 light_specular;
uniform vec4 mat_ambience;
uniform vec4 mat_diffuse;
uniform vec4 mat_specular;
uniform float mat_ideal;
uniform vec3 cam_pos;

/*
 * NOTE ON SELECTING SAMPLERS:
 * When indexing by floats, give some wiggle room instead of checking
 * values on the nose (ie, instead of v_t_id == 0.f, check whether it
 * is between -.1 and .1). This may be done by either range checking
 * or the other common method of adding .5 then calling floor(). I
 * personally do not see how the latter could possibly be faster than
 * ranged checking (which minorly increases branching).
 */
uniform sampler2DArray textures;
uniform sampler2DArray fonts;
uniform float time;
void main()
{
	float intensity = 1.0;
	frag_color = v_color;


	/*
	 * Render textures:
	 * v_t_id ~= 0: use uniform sampler2DArray textures.
	 */
	if (v_t_id > -.5f && v_t_id < .5f) {
//		frag_color = v_color * vec4(texture(textures, vec3(v_uv, v_t_index)).rgb, 1.0);
		frag_color.xyz *= texture(textures, vec3(v_uv, v_t_index)).rgb;

	/*
	 * Render fonts:
	 * v_t_id ~= 1: use uniform sampler2DArray fonts.
	 * v_t_id ~= 2: use uniform sampler2DArray fonts2.
	 */
	} else if (v_t_id > .5f && v_t_id < 1.5f) {
		float a = texture(fonts, vec3(v_uv, v_t_index)).r;

		/* Don't render the glyph's bounding box border. */
		if (a < .6f) // I got this by trial and error...
			discard;
		else {
//			frag_color.w *= a;
//			frag_color = v_color;
			frag_color.w *= a;
		}

	/* No texture selected, so render v_color only. */
	} else {
		/* Blinn-Phong shading */

		vec3 n = normalize(v_normal.xyz);
		vec3 light_dir = normalize(light_pos.xyz - v_pos.xyz);
//		vec3 light_dir = normalize(cam_pos.xyz - v_pos.xyz);
		float d = max(dot(light_dir, n), 0.f);

//		vec4 diffuse = (d > 0.f) ? d * light_diffuse * mat_diffuse : vec4(0., 0., 0., 1.);
		vec4 diffuse = (d > 0.f) ? d * light_diffuse * v_color : vec4(0., 0., 0., 1.);
		vec3 v_half = normalize(normalize(cam_pos.xyz - v_pos.xyz) + light_dir);
		d = max(0.f, dot(n, v_half));
		vec4 spec = (d > 0.f) ? pow(d, mat_ideal) * light_specular * mat_specular : vec4(0,0,0,1);

		frag_color = (diffuse + spec + mat_ambience * light_ambience);


//		vec4 tmp_color = (diffuse + spec + mat_ambience * light_ambience);
//		frag_color = vec4(tmp_color.r * (cos(4.f + time) + 1.f) * .5,
//				  tmp_color.g * (sin(8.f + time) + 1.f) * .5,
//				  tmp_color.b * (cos(2.f + time) + 1.f) * .5,
//				  tmp_color.a * .5f);
//
	}
}
