#ifndef _BATCH_H_
#define _BATCH_H_

#include <vector>
#include <math/mat.h>
#include <math/quat.h>
#include <graphics/renderable.h>

using std::vector;

namespace dengine {

/*
 * class Batch:
 * Batches contain both a vector of renderables and a vector of child batches
 * in a hierarchy to ensure proper transformations of renderables belonging
 * to sub-batches. Each sub-batch's transformation matrix is multiplied by
 * the parent batch's matrix upon addition to the parent batch, ensuring
 * the proper order of transformations. As with other classes in Dengine,
 * a destroy method is defined to enable deletion of objects in the event
 * that the Batch was declared on the stack.
 */
template <class T>
class Batch {
public:
	Batch() {};
	~Batch() { destroy(); }

	/* destroy(): deletes and clears the renderables and child batches. */
	void destroy()
	{
		for (int i = 0; i < _children.size(); i++)
			delete _children[i];

		_children.clear();

		for (int i = 0; i < _renderables.size(); i++)
			delete _renderables[i];

		_renderables.clear();
	}

	/* Appends a T renderable to the batch's renderables vector. */
	inline void add(T *renderable)
	{
		if (renderable)
			_renderables.push_back(renderable);
	}

	/* Appends a Batch of T renderables to the batch's child vector. */
	void add(Batch<T> *batch)
	{
		if (batch)
			_children.push_back(batch);
	}

	/* Removes, but does not deallocate, the batch from the batch tree. */
	void remove(const Batch<T> *batch)
	{
		for (unsigned int i = 0; i < _children.size();) {
			if (_children[i] == batch) {

				/* Preserves batch order. */
				_children[i] = NULL;
				_children.erase(_children.begin() + i);
			} else {
				_children[i++]->remove(batch);
			}
		}
	}

	/* Removes, but does not deallocate, t from the batch tree. */
	void remove(const T *t)
	{
		for (unsigned int i = 0; i < _renderables.size();) {
			if (_renderables[i] == t) {

				/* Preserve render order. */
				_renderables[i] = NULL;
				_renderables.erase(_renderables.begin() + i);
			} else {
				i++;
			}
		}

		for (unsigned int i = 0; i < _children.size(); i++)
			_children[i]->remove(t);
	}

	/*
	 * find(Batch<T>*):
	 * Returns NULL if the batch is not in the tree and a pointer to it,
	 * otherwise.
	 */
	Batch<T> *find(const Batch<T> *batch) const
	{
		Batch<T> *ret;

		if (!batch)
			return NULL;

		for (unsigned int i = 0; i < _children.size(); i++) {

			/* Check against the child batches. */
			if (_children[i] == batch)
				return _children[i];

			/* Check against the children's child batches. */
			if (ret = _children[i]->find(batch))
				return ret;
		}

		return NULL;
	}

	/*
	 * find(T*):
	 * Returns NULL if the renderable is not in the tree and a pointer
	 * to its owning batch otherwise.
	 */
	Batch<T> *find(const T *renderable) const
	{
		Batch<T> *ret;

		if (!renderable)
			return NULL;

		for (unsigned int i = 0; i < _renderables.size(); i++) {
			if (_renderables[i] == renderable)
				return this;
		}

		for (unsigned int i = 0; i < _children.size(); i++) {
			if (ret = _children[i].find(renderable))
				return ret;
		}

		return NULL;
	}

	inline unsigned int get_size() const
	{
		return _renderables.size();
	}

	/*
	 * Variables are public because they must be manipulated directly.
	 * Otherwise, getter and setter methods could be utilized to the
	 * same effect, but with the accompanying and unnecessary bloat.
	 */
	mat4 _transform;
	vector<T *> _renderables;
	vector<Batch<T> *> _children;
};

} /* namespace dengine */

#endif /* _BATCH_H_ */
