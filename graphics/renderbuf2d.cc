#include <graphics/renderbuf2d.h>

namespace dengine {

Renderbuf2D::Renderbuf2D() : _ibo(0),
			     _vao(0),
			     _vbo(0),
			     _buf(NULL),
			     _sub_count(0),
			     _draw_calls(0),
			     _num_indices(0),
			     _sub_indices(0)

{
	glGetIntegerv(GL_MAX_ELEMENTS_VERTICES, &_num_vertices);
	glGetIntegerv(GL_MAX_ELEMENTS_INDICES, &_num_indices);
}

Renderbuf2D::~Renderbuf2D()
{
	destroy();
}

void Renderbuf2D::destroy()
{
	if (_ibo) {
		glDeleteBuffers(1, &_ibo);
		_ibo = 0;
       }

	if (_vbo) {
		glDeleteBuffers(1, &_vbo);
		_vbo = 0;
	}

	if (_vao) {
		glDeleteVertexArrays(1, &_vao);
		_vao = 0;
	}

	_num_vertices = 0;
	_num_indices = 0;

	/* Delete textures. */
	for (set_iter it = _tslots.begin(); it != _tslots.end(); it++)
		delete *it;
	_tslots.clear();
}

void Renderbuf2D::begin()
{
	if (!_vao || !_vbo || !_ibo) {
		fprintf(stderr, "Renderbuf2D not initialized.\n");
		return;
	}

	glBindBuffer(GL_ARRAY_BUFFER, _vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ibo);
	_buf = (Vertex3 *)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

	_sub_count = 0;
	_sub_indices = 0;
}

void Renderbuf2D::end()
{
	if (!_vao || !_vbo || !_ibo) {
		fprintf(stderr, "Renderbuf2D not initialized.\n");
		return;
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glUnmapBuffer(GL_ARRAY_BUFFER);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Renderbuf2D::flush()
{
	if (!_vao || !_vbo || !_ibo) {
		fprintf(stderr, "Renderbuf2D not initialized.\n");
		return;
	}

	_draw_calls++;

	for (set_iter it = _tslots.begin(); it != _tslots.end(); it++)
		(*it)->bind();

	glBindVertexArray(_vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ibo);

	glDrawElements(GL_TRIANGLES, _sub_indices, GL_UNSIGNED_INT, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	for (set_iter it = _tslots.begin(); it != _tslots.end(); it++)
		(*it)->unbind();
}

void Renderbuf2D::init(const vector<GLuint> *all_indices)
{
	destroy();

	glGenVertexArrays(1, &_vao);
	glGenBuffers(1, &_vbo);
	glBindVertexArray(_vao);
	glBindBuffer(GL_ARRAY_BUFFER, _vbo);

	/*
	 * Specify the size of the VBO, its location, and how to use it.
	 * GL_ARRAY_BUFFER essentially specifies that the VBO is part of
	 * a VAO.
	 */
	glBufferData(GL_ARRAY_BUFFER, BUFFER_SIZE, 0, GL_STATIC_DRAW);

	glEnableVertexAttribArray(POSITION_INDEX);
	glEnableVertexAttribArray(UV_INDEX);
	glEnableVertexAttribArray(TID_INDEX);
	glEnableVertexAttribArray(TIDX_INDEX);
	glEnableVertexAttribArray(COLOR_INDEX);

	/* Position coordinate data. */
	glVertexAttribPointer(POSITION_INDEX,
			      3,
			      GL_FLOAT,
			      GL_FALSE,
			      VERTEX_SIZE,
			      POSITION_OFFSET);

	/* Texture coordinate data. */
	glVertexAttribPointer(UV_INDEX,
			      2,
			      GL_FLOAT,
			      GL_FALSE,
			      VERTEX_SIZE,
			      UV_OFFSET);

	/* Texture Array ID (specifies which sampler2DArray to use). */
	glVertexAttribPointer(TID_INDEX,
			      1,
			      GL_FLOAT,
			      GL_FALSE,
			      VERTEX_SIZE,
			      TID_OFFSET);

	/* Texture index (specifies the texture within the sampler2DArray). */
	glVertexAttribPointer(TIDX_INDEX,
			      1,
			      GL_FLOAT,
			      GL_FALSE,
			      VERTEX_SIZE,
			      TIDX_OFFSET);

	/* Vertex color. */
	glVertexAttribPointer(COLOR_INDEX,
			      4,
			      GL_UNSIGNED_BYTE,
			      GL_TRUE,
			      VERTEX_SIZE,
			      COLOR_OFFSET);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	/* Create and fill the index buffer. */
	if (_ibo)
		glDeleteBuffers(1, &_ibo);

	glGenBuffers(1, &_ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ibo);

	/* Load custom indices into the index buffer object. */
	if (all_indices) {
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			     all_indices->size() * sizeof(GLuint),
			     (GLuint *)&all_indices[0],
			     GL_STATIC_DRAW);

		_sub_indices = all_indices->size();

	/*
	 * Generate indices with the assumption that each 2D renderable
	 * has 4 vertices (2 triangles).
	 */
	} else {
		GLuint *indices = new GLuint[INDICES_SIZE];
		int offset = 0;
		for (int i = 0; i < INDICES_SIZE; i += 6, offset += 4) {
			indices[i]     = offset;
			indices[i + 1] = offset + 1;
			indices[i + 2] = offset + 2;
			indices[i + 3] = offset + 2;
			indices[i + 4] = offset + 3;
			indices[i + 5] = offset;
		}

		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			     INDICES_SIZE * sizeof(GLuint),
			     indices,
			     GL_STATIC_DRAW);
		delete [] indices;

		_sub_indices = INDICES_SIZE;
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}

void Renderbuf2D::insert_texture(Texture *t)
{
	if (t) {
		set_iter it = _tslots.find(t);
		if (it == _tslots.end())
			_tslots.insert(t);
	}
}

} /* namespace dengine */