#include "dengine.h"

using dengine::Dengine;

int main(int argc, char **argv)
{
	Dengine dengine(argc, argv);
	dengine.run();

	return 0;
}
