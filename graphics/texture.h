#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include <GL/glew.h>

namespace dengine {

class Texture {
public:
	Texture();
	virtual ~Texture();

	virtual void bind() = 0;
	virtual void unbind() = 0;
	inline const GLuint get_id() const { return _id; }
	inline const float get_width() const { return _width; }
	inline const float get_height() const { return _height; }
	inline const int get_offset() const { return _offset; }

protected:
	GLuint _id;
	int _offset;	/* Offset in shader. */
	int _bound;	/* _bound = 0 if unbound, 1 if bound */
	float _width, _height, _depth;
};

} /* namespace dengine */

#endif /* _TEXTURE_H_ */