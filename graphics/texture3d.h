#ifndef _TEXTURE3D_H_
#define _TEXTURE3D_H_

#include <vector>
#include <io/io.h>
#include <graphics/texture.h>

using std::vector;

namespace dengine {

class Texture3D : public Texture {
public:
	Texture3D() {};
	Texture3D(vector<ImageData *>& images,
		  int max_w = 0,
		  int max_h = 0,
		  int layout_offset = 0);
	int init(vector<ImageData *>& images,
		 int max_w = 0,
		 int max_h = 0,
		 int layout_offset = 0);
	virtual void bind();
	virtual void unbind();
};

} /* namespace dengine */

#endif /* _TEXTURE3D_H_ */
