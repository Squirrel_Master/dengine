#include "dengine.h"

#include <sys/statvfs.h>
#include <sys/types.h>
#include <sys/sysinfo.h>

namespace dengine {

Dengine *Dengine::instance = NULL;

Dengine::Dengine() : _shader(NULL),
		     _time(0.f),
		     _x(0),
		     _y(0),
		     _w(960.f),
		     _h(540.f),
		     _aw(16.f),
		     _ah(9.f),
		     _frames(0),
		     _mainwindow(0),
		     _curr(0),
		     _prev(0)
{
	instance = this;
}

Dengine::Dengine(int argc,
		 char **argv,
		 int x,
		 int y,
		 float w,
		 float h,
		 float aw,
		 float ah) : _shader(NULL),
			     _time(0.f),
			     _frames(0),
			     _mainwindow(0),
			     _curr(0),
			     _prev(0)
{
	instance = this;
	init(argc, argv, x, y, w, h, aw, ah);
}

Dengine::~Dengine()
{
        destroy();
}

void Dengine::destroy()
{
        renderer.destroy();
}

/* Initializes glut and glew and creates a window. */
void Dengine::init(int argc,
		   char **argv,
		   int x,
		   int y,
		   float w,
		   float h,
		   float aw,
		   float ah)
{
	/* Initialize glut. */
	glutInit(&argc, argv);

        _prev = glutGet(GLUT_ELAPSED_TIME);

        /* Initialize the display mode. */
        glutInitDisplayMode(GLUT_RGBA |
                            GLUT_DEPTH |
                            GLUT_DOUBLE |
                            GLUT_MULTISAMPLE);

        /*
         * Specify that control returns to the function which called
         * glutMainLoop().
         */
        glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE,
                      GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	/* Create and title a window. */
	_x = x < 0 ? -x : x;
	_y = y < 0 ? -y : y;
	if (w > 0.f)
		_w = w;
	if (h > 0.f)
		_h = h;
	if (aw > 0.f)
		_aw = aw;
	if (ah > 0.f)
		_ah = ah;

	_camera.set_projection(_aw, _ah, 0.001f, 10.f, 45.f, PERSPECTIVE);
	_camera.set_position(vec3(0, 1, 3.f));
	_camera.set_cam_type(FPS);

	glutInitWindowPosition(_x, _y);
	glutInitWindowSize(_w, _h);
	_mainwindow = glutCreateWindow(argv[0]);

        /* Set the background color to black. */
	glClearColor(0.f, 0.f, 0.f, 1.f);

	/* Cull all clockwise faces. */
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);

	/* Enable the z-buffer for hidden surface removal. */
	glEnable(GL_DEPTH_TEST);

	/* Enable blending, necessary for rendering fonts (alphas). */
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/* Gamma correction. */
	glEnable(GL_FRAMEBUFFER_SRGB);

#ifndef __APPLE__
	/* Initialize the extension manager if NOT Apple machine. */
	glewInit();
#endif

	printf("OpenGL Version: %s\n", glGetString(GL_VERSION));
	printf("OpenGL Vendor: %s\n", glGetString(GL_VENDOR));
	printf("OpenGL Renderer: %s\n", glGetString(GL_RENDERER));

	int maxes;
	glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &maxes);
	printf("Maximum array texture layers: %d\n", maxes);

	glGetIntegerv(GL_MAX_ELEMENTS_VERTICES, &maxes);
	printf("Maximum vertex array vertices: %d\n", maxes);

	glGetIntegerv(GL_MAX_ELEMENTS_INDICES, &maxes);
	printf("Maximum vertex array indices: %d\n", maxes);

	glGetIntegerv(GL_MAX_DRAW_BUFFERS, &maxes);
	printf("Maximum draw buffers: %d\n", maxes);

	glutSetCursor(GLUT_CURSOR_NONE);
	glutWarpPointer(_w / 2, _h / 2);

	/* Create and compile the shaders. */
        _shader = new Shader();

        if (_shader->init("graphics/shader_v.glsl",
                             "graphics/shader_f.glsl") < 0) {
		destroy();
                return;
	}

	/* Initialize the renderers. */
	renderer.init();

	/* Add the shaders to the appropriate renderers. */
	renderer.add_shader(_shader);

        /* Add the attributes to the shaders. */
	_shader->add_attribute("position");
	_shader->add_attribute("normal");
	_shader->add_attribute("tangent");
	_shader->add_attribute("uv");
	_shader->add_attribute("t_id");
	_shader->add_attribute("t_index");
	_shader->add_attribute("color");

	/* Link the shaders. */
	_shader->link();

	/* Set the uniforms for the shaders. */
	_shader->enable();
	_shader->set_uniform_mat4("model_matrix", translation(vec3(0., 0., 0.)), GL_TRUE);
	_shader->set_uniform_4f("color", vec4(.2f, .3f, .8f, 1.f));
	_shader->set_uniform_4f("light_pos", vec4(0.f, 10.f, 0.f, 0.f));
	_shader->set_uniform_4f("light_ambience", vec4(.0f, .0f, .0f, 1.f));
	_shader->set_uniform_4f("light_diffuse", vec4(1.f, 1.f, 1.f, 1.f));
	_shader->set_uniform_4f("light_specular", vec4(1.f, 1.f, 1.f, 1.f));
	_shader->set_uniform_4f("mat_ambience", vec4(0.f, 0.f, 0.f, 1.f));
	_shader->set_uniform_4f("mat_diffuse", vec4(1.f, .8f, 0.f, 1.f));
	_shader->set_uniform_4f("mat_specular", vec4(1.f, .8f, 0.f, 1.f));
	_shader->set_uniform_1f("mat_ideal", 100.f);
	_shader->set_uniform_3f("cam_pos", _camera.get_pos());

	_camera.set_shader(_shader, "VP_matrix");

	/*
	 * Only 1 array texture currently, may expand in the future, thus
	 * using set_uniform_1iv and sending array of indices for multiple
	 * samplers.
	 */
	_shader->set_uniform_1i("textures", 0);
	_shader->set_uniform_1i("fonts", 1);

	_camera.set_speed(.01);

	/* Add images and create a TextureArray of them. */
	ImageData *image = new ImageData("ext/images/cause_of_death2.jpg", 24);
	ImageData *image2 = new ImageData("ext/images/bruegel_hunters"
					  "_in_the_snow.jpg",
					  24);
	ImageData *image3 = new ImageData("ext/images/trump_it.png", 24);

	vector<ImageData *> images;
	images.push_back(image);
	images.push_back(image2);
	images.push_back(image3);

	Texture *texture_images = new TextureArray(images, 0, 0, 0);

	/* Add fonts and create Texture2D_Arrays of them. */
	_fontmanager.add_font("Vera", "./ext/fonts/Vera.ttf", 100);
	_fontmanager.add_font("SourceCodePro-Regular",
				"./ext/fonts/SourceCodePro-Regular.ttf", 100);
	_fontmanager.add_font("LuckiestGuy",
				"./ext/fonts/LuckiestGuy.ttf", 100);
	/* Build the first Texture2D_Array utilizing the above 3 fonts. */
	if (_fontmanager.build_texture_array(1) < 0) {
		printf("Unable to build font texture array.\n");
		destroy();
		return;
	}

	Batch<Renderable> *batch = new Batch<Renderable>();
	batch->add(new Sprite(vec3(0, 0, .01),
			       vec2(2.5, 2.5),
			       vec4(1, 1, 1., 1.),
				texture_images,
			       1,
			       image2));
	Text *fps = new Text("FPS: ",
			_fontmanager.get_font("Vera"),
			vec3(-1,0,1),
			vec2(1.f, 1.f),
			vec2(_aw, _ah),
			vec4(1.f, 1.f, 1.f, 1.f));
	batch->add(fps);

	/*
	 * Create a batch of thousands of Sprites, each of which consists of
	 * a texture within the Texture2D_Array created from the images.
	 */
	Batch<Renderable> *background_batch = new Batch<Renderable>();
	int countem = 0;
	for (float y = -_ah; y < _ah; y += .1f) {
		for (float x = -_aw; x < _aw; x += .1f) {
			countem++;
			int tex_idx = rand() % 3;
			ImageData *im;
			if (tex_idx == 0)
				im = image;
			else if (tex_idx == 1)
				im = image2;
			else
				im = image3;

                        background_batch->add(new Sprite(vec3(x, y, 0),
				      vec2(.1f, .1f),
                               	      vec4(rand() % 1000/1000.f,
					   0,
					   1,
					   1),
					texture_images,
				      tex_idx,
				      im));
		}
	}

	/*
	 * Right hand rule, x is thumb, y index finger, z middle, a rotation
	 * of 90 about x-axis points index finger in your face.
	 */
	background_batch->_transform = rotation(-90.f, vec3(1, 0, 0));

	printf("# of background batch sprites: %d\n", countem);

	/* Render order:
	 *	for each batch in the renderer
	 *		for each child batch (recursive, depth first)
	 *		for each renderable
	 * Therefore, add what you want to see first (renderable) to the base
	 * of a batch and add that batch FIRST to the renderer.
	 */

	/* _batch will render last since it is the last added. */
	renderer.add(batch);
	renderer.add(background_batch);

	/* No longer need the image data, so delete it. */
	for (int i = 0; i < images.size(); i++)
		delete images[i];

	Mesh *kitten = new Mesh("./tests/kitten.obj", vec4(1.f, 0.f, 1.f, 1.f));
	Mesh *bunny = new Mesh("./tests/bunny_regular.obj", vec4(.5f, 1.f, .25f, 1.f));
	Batch<Renderable> *tmp_batch = new Batch<Renderable>();
	for (int i = -10; i < 11; i++) {
		Mesh *m = i % 5 == 0 ? new Mesh(bunny) : new Mesh(kitten);
		m->transform(translation(2 * i, 1, -2));
		tmp_batch->add(m);
	}

	delete kitten;
	delete bunny;
	renderer.add(tmp_batch);

	/*
	 * Specify the glut callback functions.
	 *
	 * Note:
	 * In general, all update code should be in glutIdleFunc rather than
	 * glutDisplayFunc.  Only rendering should appear in glutDisplayFunc.
	 */
        glutDisplayFunc(display_wrapper);
        glutIdleFunc(idle_wrapper);
        glutReshapeFunc(resize_wrapper);
        glutMotionFunc(mouse_motion_wrapper);
	glutPassiveMotionFunc(mouse_passive_motion_wrapper);
        glutMouseFunc(mouse_button_wrapper);
        glutKeyboardFunc(key_down_wrapper);
        glutKeyboardUpFunc(key_up_wrapper);
        glutSpecialFunc(key_special_down_wrapper);
        glutSpecialUpFunc(key_special_up_wrapper);
	glutCloseFunc(close_window_wrapper);
}

void Dengine::run()
{
	if (glutGet(GLUT_INIT_STATE) == 1)
		glutMainLoop();
}

void Dengine::display()
{
	static float mod_time = 0.f;

	process_keys();

	/* Set the base depth to 1.0 */
	glClearDepth(1.0);

	/* Clear the window (with white) and clear the z-buffer. */
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	_shader->enable();
	_shader->set_uniform_3f("cam_pos", _camera.get_pos());
//	_shader->set_uniform_1f("time", mod_time);

	_camera.update();

//	glEnable(GL_CULL_FACE);
	renderer.render();
//	glDisable(GL_CULL_FACE);

	_shader->disable();

	/*
	 * Move the drawn buffer onto the screen and give access to
	 * the previous buffer (which was replaced).
	 */
        update_window();

	mod_time += .05f;
}

void Dengine::idle()
{
	process_keys();
        _frames++;

	_curr = glutGet(GLUT_ELAPSED_TIME);
	_time = _curr - _prev;
        if (_time > 1000) {
		int fps = (int)(_frames * 1000.f) / _time;
		char buf[10];
		printf("fps: %d\n", fps);
                _prev = _curr;
                _frames = 0;
        }

        glutPostRedisplay();
}

int Dengine::find_key_state(unsigned char c)
{
	unordered_map<unsigned char, int>::iterator it;
	it = _key_states.find(c);
	if (it != _key_states.end())
		return it->second;
	return 0;
}

void Dengine::process_keys()
{
	/* Escape key */
	if (find_key_state(27) == 1) {
		glutLeaveMainLoop();
        }
	if (find_key_state('w') == 1) {
		_camera.move(FRONT);
	}
	if (find_key_state('a')) {
		_camera.move(LEFT);
	}
	if (find_key_state('s')) {
		_camera.move(BACK);
	}
	if (find_key_state('d')) {
		_camera.move(RIGHT);
	}
}

/*
 * key_down:
 * The keyboard callback function that is called whenever a normal ASCII key is
 * pressed.
 */
void Dengine::key_down(unsigned char key, int x, int y)
{
	unordered_map<unsigned char, int>::iterator it = _key_states.find(key);
	if (it == _key_states.end())
		_key_states.insert({key, 1});
	else
		it->second = 1;

}

/*
 * key_up:
 * The keyboard callback function that is called whenever a normal ASCII key is
 * released.
 */
void Dengine::key_up(unsigned char key, int x, int y)
{
	unordered_map<unsigned char, int>::iterator it = _key_states.find(key);
	if (it == _key_states.end())
		_key_states.insert({key, 0});
	else
		it->second = 0;
}

/*
 * key_special_down:
 * The keyboard callback function that is called whenever a special key
 * is pressed.
 */
void Dengine::key_special_down(int key, int x, int y)
{
	unordered_map<int, int>::iterator it = _key_special_states.find(key);
	if (it == _key_special_states.end())
		_key_special_states.insert({key, 1});
	else
		it->second = 1;
}

/*
 * key_special_up:
 * The keyboard callback function that is called whenever a special key
 * is released.
 */
void Dengine::key_special_up(int key, int x, int y)
{
	unordered_map<int, int>::iterator it = _key_special_states.find(key);
	if (it == _key_special_states.end())
		_key_special_states.insert({key, 0});
	else
		it->second = 0;
}

/*
 * mouse_motion:
 * The mouse motion callback function that is called when the mouse moves
 * within the window while one or more mouse buttons are pressed.
 */
void Dengine::mouse_motion(int x, int y)
{

	static float pitch = 0;
	static float yaw = 0;

	float delta_yaw = (x - _x) * .5;
	float delta_pitch = (y - _y) * .5;

	if (abs(delta_yaw) < 50) {
		yaw += delta_yaw;
		if (yaw > 360)
			yaw -= 360;
		else if (yaw < 0)
			yaw += 360;
	}
	if (abs(delta_pitch) < 50) {
		pitch += delta_pitch;
		if (pitch > 90)
			pitch = 90;
		else if (pitch < -90)
			pitch = -90;
	}

	_x = x;
	_y = y;

	if (abs(_x - (_w / 2)) > _w / 3) {
		_x = _w / 2;
		glutWarpPointer(_x, _y);
	} else if (abs(_y - (_h / 2)) > _h / 3) {
		_y = _h / 2;
		glutWarpPointer(_x, _y);
	}

	_camera.rotate(deg_to_rad * pitch, deg_to_rad * yaw, 0, ZYX);

	pitch = 0;
	yaw = 0;
}

/*
 * mouse_passive_motion:
 * The mouse motion callback function that is called when the mouse moves
 * within the window while no mouse buttons are pressed.
 */
void Dengine::mouse_passive_motion(int x, int y)
{
	mouse_motion(x, y);
}

/*
 * mouse_button:
 * The mouse button callback function that is called on mouse button presses.
 */
void Dengine::mouse_button(int button, int state, int x, int y)
{
/*	switch (button) {
	case GLUT_LEFT_BUTTON:
		mouse_states[0] = state == GLUT_UP ? 0 : 1;
		break;
	case GLUT_MIDDLE_BUTTON:
		mouse_states[1] = state == GLUT_UP ? 0 : 1;
		break;
	case GLUT_RIGHT_BUTTON:
		mouse_states[2] = state == GLUT_UP ? 0 : 1;
		break;
*/
	/* Mouse wheel */
/*	case 3:
		if (state == GLUT_DOWN)
			_camera.zoom(-.01f);
		break;
	case 4:
		 if (state == GLUT_DOWN)
			_camera.zoom(.01f);
		break;
	}
*/
	unordered_map<int, int>::iterator it = _mouse_states.find(button);
	if (it == _mouse_states.end())
		_mouse_states.insert({button, state == GLUT_UP ? 0 : 1});
	else
		it->second = state == GLUT_UP ? 0 : 1;

	if (button == 3 && state == GLUT_DOWN)
		_camera.zoom(-.01f);
	else if (button == 4 && state == GLUT_DOWN)
		_camera.zoom(.01f);
}

/*
 * resize:
 * The callback function for resizing the window.
 */
void Dengine::resize(int w, int h)
{
        if (w > 0)
                _w = w;

        if (h > 0)
                _h = h;

	glViewport(0, 0, _w, _h);
}

/*
 * update_window:
 * Swap the back with the front draw buffer.
 */
void Dengine::update_window()
{
	GLenum err = glGetError();
	if (err != GL_NO_ERROR)
		fprintf(stderr, "OpenGL error: %d\n", err);

	glutSwapBuffers();
}

/* Wrappers for sending the Dengine functions to glut. */
void Dengine::display_wrapper()
{
	instance->display();
}

void Dengine::idle_wrapper()
{
        instance->idle();
}

void Dengine::resize_wrapper(int w, int h)
{
        instance->resize(w, h);
}

void Dengine::key_down_wrapper(unsigned char key, int x, int y)
{
	instance->key_down(key, x, y);
}

void Dengine::key_up_wrapper(unsigned char key, int x, int y)
{
	instance->key_up(key, x, y);
}

void Dengine::key_special_down_wrapper(int key, int x, int y)
{
	instance->key_special_down(key, x, y);
}

void Dengine::key_special_up_wrapper(int key, int x, int y)
{
	instance->key_special_up(key, x, y);
}

void Dengine::mouse_motion_wrapper(int x, int y)
{
	instance->mouse_motion(x, y);
}

void Dengine::mouse_passive_motion_wrapper(int x, int y)
{
	instance->mouse_passive_motion(x, y);
}

void Dengine::mouse_button_wrapper(int button, int state, int x, int y)
{
	instance->mouse_button(button, state, x, y);
}

void Dengine::close_window_wrapper()
{
        glutLeaveMainLoop();
}


} /* namespace dengine */
