#ifndef _DENGINE_H_
#define _DENGINE_H_

#include <stdio.h>
#include <string.h>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/freeglut_ext.h>

#include <math/dmath.h>
#include <graphics/sprite.h>
#include <graphics/shader.h>
#include <graphics/renderer2d.h>
#include <graphics/renderer3d.h>
#include <graphics/batch.h>
#include <graphics/texture.h>
#include <graphics/texture_array.h>
#include <graphics/text.h>
#include <graphics/font_manager.h>
#include <SFML/Audio.hpp>
#include <graphics/mesh.h>

#include <math/quat.h>
#include <graphics/camera.h>
#include <unordered_map>

namespace dengine {

using std::unordered_map;

/*
 * Dengine:
 * <describe Dengine class here>
 * NOTE: Key states kept in unordered maps to account for goofy or otherwise
 * unexpected input keys.
 */

class Dengine {
public:
	Dengine();
	Dengine(int argc,
		  char **argv,
		  int x = 0,		/* Window x position coordinate */
		  int y = 0,		/* Window y position coordinate */
		  float w = 960.f,	/* Window width */
		  float h = 540.f,	/* Window height */
		  float aw = 16.f,	/* Aspect width */
		  float ah = 9.f);	/* Aspect height */
	~Dengine();

	void init(int argc,
		  char **argv,
		  int x = 0,		/* Window x position coordinate */
		  int y = 0,		/* Window y position coordinate */
		  float w = 960.f,	/* Window width */
		  float h = 540.f,	/* Window height */
		  float aw = 16.f,	/* Aspect width */
		  float ah = 9.f);	/* Aspect height */

	void run();

private:
	void destroy();
        void display();
	int find_key_state(unsigned char c);
        void process_keys();
        void key_down(unsigned char key, int x, int y);
        void key_up(unsigned char key, int x, int y);
        void key_special_down(int key, int x, int y);
        void key_special_up(int key, int x, int y);
        void mouse_motion(int x, int y);
	void mouse_passive_motion(int x, int y);
        void mouse_button(int button, int state, int x, int y);
        void resize(int w, int h);
        void update_window();
        void idle();

	/* Wrappers for Dengine functions to the Glut callback functions. */
	static void close_window_wrapper();
        static void display_wrapper();
        static void resize_wrapper(int width, int height);
        static void key_down_wrapper(unsigned char key, int x, int y);
        static void key_up_wrapper(unsigned char key, int x, int y);
        static void key_special_down_wrapper(int key, int x, int y);
        static void key_special_up_wrapper(int key, int x, int y);
        static void mouse_motion_wrapper(int x, int y);
	static void mouse_passive_motion_wrapper(int x, int y);
        static void mouse_button_wrapper(int button, int state, int x, int y);
        static void idle_wrapper();

	/* Maintain a pointer to the Dengine instance (for callbacks). */
	static Dengine *instance;

        /* 256 ASCII key states */
	unordered_map<unsigned char, int> _key_states;

        /*
	 * Special key states, including, but not limited to: F1-F12, arrow
	 * keys, PAGE UP, PAGE DOWN, HOME, END, INSERT, CTRL, ALT, and SHIFT.
         */
	unordered_map<int, int> _key_special_states;

        /* Mouse button states, including the wheel. */
	unordered_map<int, int> _mouse_states;

	GLuint _mainwindow;
        Shader *_shader;
	Renderer3D renderer;
	FontManager _fontmanager;
	Camera _camera;

	int _x, _y;	/* Temporary variables for testing. */
	int _w, _h;	/* Width and height of the window. */
	int _aw, _ah;	/* Aspect width and height. */

	float _time;
	int _frames, _curr, _prev;
};

} /* namespace dengine */

#endif /* _DENGINE_H_ */
