#Executable Name
EXE = test

#Compiler:
CXX = g++ #clang++ or gcc

#Location of headers:
INCLUDES = -I. -I./ext/freetype-2.7.1/include -I./graphics -I./math -I./io

#Compiler flags:
CXXFLAGS = -g -std=c++11 -Wl,--no-as-needed -O3

#Location of libraries
LDFLAGS = -g -L/usr/lib/x86_64-linux-gnu -L/usr/lib

#Libraries:
LDLIBS = -lglut -lGL -lGLU -lGLEW -lm -lfreeimage -lopenal -lfreetype \
	 -lpthread -lsfml-system -lsfml-audio

#Dependency options:
DEPENDENCY_OPTIONS = -MM

#-- Do not edit below this line --

# Subdirs to search for additional source files
SUBDIRS := $(shell ls -F | grep "\/" )
DIRS := ./ $(SUBDIRS)
SOURCE_FILES := $(foreach d, $(DIRS), $(wildcard $(d)*.cc) )

# Create an object file of every cpp file
OBJECTS = $(patsubst %.cc, %.o, $(SOURCE_FILES))

# Dependencies
DEPENDENCIES = $(patsubst %.cc, %.d, $(SOURCE_FILES))

# Create .d files
%.d: %.cc
	$(CXX) $(DEPENDENCY_OPTIONS) $< -MT "$*.o $*.d" -MF $*.d

# Make $(EXE) the default target
$(EXE): $(OBJECTS)
	$(CXX) -o $(EXE) $(OBJECTS) $(LDFLAGS) $(LDLIBS)

# Compile every cpp file to an object
%.o: %.cc
	$(CXX) -c $(CXXFLAGS) -o $@ $< $(INCLUDES)

# Build & Run Project
run: $(EXE)
	./$(EXE)

# Clean & Debug
.PHONY: makefile-debug
makefile-debug:

.PHONY: clean
clean:
	rm -f $(EXE) $(OBJECTS) $(DEPENDENCIES)

.PHONY: all
all: clean $(DEPENDENCIES) $(EXE)

.PHONY: depclean
depclean:
	rm -f $(OBJECTS) $(DEPENDENCIES)

clean-all: clean depclean
