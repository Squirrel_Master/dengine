#include <io/io.h>

namespace dengine {

/*
 * read_wavefront:
 * Parses a wavefront obj file and saves the results in the referenced
 * vector arguments.  Returns -1 on error, 0 on success.
 *
 * NOTE: Currently, normals, tangents, and texture coordinates (uv) are
 * ignored.
 */
int read_wavefront(const char *filename,
		   vector<vec3> *vertices,
		   vector<unsigned int> *indices,
		   vector<vec3> *normals,
		   vector<vec3> *tangents,
		   vector<vec2> *uv)
{
	if (!filename || !vertices || !indices)
		return -1;

	vertices->clear();
	indices->clear();

        char buf[BUF_SIZE] = {0};
        int ret = -1;

        if (!filename)
                return ret;

        FILE *file = fopen(filename, "r");
        if (!file) {
                printf("Unable to open %s.\n", filename);
                return ret;
        }

        while(fgets(buf, sizeof(buf), file)) {
                char *token = strtok(buf, " ");
                if (buf[0] == 'v') {
                        double pa, pb, pc;
                        if (!(token = strtok(NULL, " ")))
                                goto err_out;
                        pa = atof(token);
                        if (!(token = strtok(NULL, " ")))
                                goto err_out;
                        pb = atof(token);
                        if(!(token = strtok(NULL, " ")))
                                goto err_out;
                        pc = atof(token);

			vertices->push_back(vec3(pa, pb, pc));
                } else if (buf[0] == 'f') {
                        int i, j, k;
                        if (!(token = strtok(NULL, " ")))
                                goto err_out;
                        i = atoi(token);
                        if (!(token = strtok(NULL, " ")))
                                goto err_out;
                        j = atoi(token);
                        if (!(token = strtok(NULL, " ")))
                                goto err_out;
                        k = atoi(token);
                        indices->push_back(i-1);
                        indices->push_back(j-1);
                        indices->push_back(k-1);
                }
        }

        ret = 0;

err_out:
        fclose(file);
        return ret;
}

int read_file(const char *filename, vector<unsigned char> &buffer)
{
	if (!filename)
		return -1;
        FILE *file = fopen(filename, "rb");
        if (!file)
                return -1;
        fseek(file, 0L, SEEK_END);
	long size = ftell(file);
        fseek(file, 0L, SEEK_SET);
        buffer.resize(size);
        fread(&(buffer[0]), 1, size, file);
        fclose(file);
        return 0;
}

char *read_file(const char *filename)
{
	if (!filename)
		return NULL;
        FILE *file = fopen(filename, "rb");
        if (!file)
                return NULL;
        fseek(file, 0L, SEEK_END);
	long size = ftell(file);
        fseek(file, 0L, SEEK_SET);
        char *buf = new char[size + 1];
        fread(buf, 1, size, file);
        buf[size] = '\0';
        fclose(file);
        return buf;
}

ImageData::ImageData() : _width(0),
			 _height(0),
			 _bpp(0),
			 _dib(NULL),
			 _pixels(NULL)
{
}

/* ================================ ImageData =============================== */
ImageData::ImageData(const char *filename, int format) : _width(0),
							 _height(0),
							 _bpp(0),
							 _dib(NULL),
							 _pixels(NULL)
{
	load(filename, format);
}

ImageData::~ImageData()
{
	unload();
}

int ImageData::load(const char *filename, int format)
{
	if (format % 8 != 0) {
		fprintf(stderr, "Format must be in 8-bit units.\n");
		return -1;
	}

	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
	fif = FreeImage_GetFileType(filename, 0);
	if (fif == FIF_UNKNOWN)
		fif = FreeImage_GetFIFFromFilename(filename);
	if (fif == FIF_UNKNOWN) {
		fprintf(stderr, "Unknown filetype.\n");
		return -1;
	}

	if (FreeImage_FIFSupportsReading(fif))
		_dib = FreeImage_Load(fif, filename);

	if (!_dib) {
		fprintf(stderr, "Unknown filetype.\n");
		return -1;
	}

	if (_bpp != format) {
		FIBITMAP *tmp;
		if (format == 24)
			tmp = FreeImage_ConvertTo24Bits(_dib);
		else if (format == 16)
			tmp = FreeImage_ConvertTo16Bits565(_dib);
		else if (format == 8)
			tmp = FreeImage_ConvertTo8Bits(_dib);
		else
			tmp = FreeImage_ConvertTo32Bits(_dib);
		FreeImage_Unload(_dib);
		_dib = tmp;
	}

	_width = FreeImage_GetWidth(_dib);
	_height = FreeImage_GetHeight(_dib);
	_bpp = FreeImage_GetBPP(_dib);
	_pixels = FreeImage_GetBits(_dib);

	return 0;
}

void ImageData::unload()
{
	if (_dib) {
		FreeImage_Unload(_dib);
		_dib = NULL;
	}
}

} /* namespace dengine */
