#ifndef _SPRITE_H_
#define _SPRITE_H_

#include <graphics/texture.h>
#include <graphics/renderbuf2d.h>
#include <graphics/vertex.h>
#include <graphics/renderable.h>
#include <io/io.h>
#include <math/dmath.h>
#include <math/mat.h>

namespace dengine {

/*=== COORDINATE LAYOUT ===|

  (x0,y1) 3-------2 (x1,y1)
          |      /|
          |    /  |
          |  /    |
          |/      |
  (x0,y0) 0-------1 (x1,y0)

Indices: [0,1,2,2,3,0]
|==========================*/

class Sprite : public Renderable2D {
public:
        Sprite() : _texture(NULL),
		    _idx(-1.f),
		    _width(0.f),
		   _height(0.f) {};
        Sprite(const vec3& position,
	       const vec2& size,
	       const vec4& color,
	       Texture *t = NULL,
	       const int texture_array_index = -1,
	       const ImageData *image = NULL);
	virtual ~Sprite();

        int init(const vec3& position,
		  const vec2& size,
		  const vec4& color,
		  Texture *t = NULL,
		  const int texture_array_index = -1,
		  const ImageData *image = NULL);
	void destroy();

	virtual void submit(Renderbuf2D *rbuf, mat4 *t);
	virtual void submit(Renderbuf3D *rbuf, mat4 *t);

	/* Getter methods. */
	inline const float get_width() const { return _width; }
	inline const float get_height() const { return _height; }
	inline const GLuint get_tid() const
	{
		return _texture ? _texture->get_id() : 0;
	}
	inline const vec2* get_uv() const { return &_uv[0]; }
	inline const Texture *get_texture() const { return _texture; }
	inline const int get_idx() const { return _idx; }

	/* Setter methods. */
	inline void set_width(float f) { _width = fabs(f); }
	inline void set_height(float f) { _height = fabs(f); }

protected:
	float _width, _height;

	vec2 _uv[4];

	/*
	 * Maintain pointer to texture, whether a Texture2D, Texture2D_Array,
	 * or Texture3D.
	 */
	Texture *_texture;

	/*
	 * _idx specifies the depth within Texture2D_Array or Texture3D, hence
	 * is -1 if using Texture2D.
	 */
	float _idx;
};

} /* namespace dengine */

#endif /* _SPRITE_H_ */
