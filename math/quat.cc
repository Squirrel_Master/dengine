/*
-----------------------------------------------------------------------------
This source file is part of OGRE
    (Object-oriented Graphics Rendering Engine)
For the latest info, see http://www.ogre3d.org/

Copyright (c) 2000-2014 Torus Knot Software Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include <math/quat.h>

namespace dengine {
	const float Quat::EPSILON = 0.0001;
	const Quat Quat::ZERO(0.f, 0.f, 0.f, 0.f);
	const Quat Quat::IDENTITY(1.f, 0.f, 0.f, 0.f);

//    const Real Quaternion::msEpsilon = 1e-03;
//    const Quaternion Quaternion::ZERO(0,0,0,0);
//    const Quaternion Quaternion::IDENTITY(1,0,0,0);

void Quat::from_rotation_matrix(const mat3& m)
{
	float trace = m[0][0] + m[1][1] + m[2][2];
	float root;
	if (trace > 0.f) {
		root = sqrt(trace + 1.f);
		w = root * .5f;
		root = .5f / root;
		x = (m[2][1] - m[1][2]) * root;
		y = (m[0][2] - m[2][0]) * root;
		z = (m[1][0] - m[0][1]) * root;
	} else {
		static int next[3] = {1, 2, 0};
		int i = 0;
		if (m[1][1] > m[0][0])
			i = 1;
		if (m[2][2] > m[i][i])
			i = 2;
		int j = next[i];
		int k = next[j];
		root = sqrt(m[i][i] - m[j][j] - m[k][k] + 1.f);
		float *tmp[3] = {&x, & y, &z};
		*tmp[i] = .5 * root;
		root = .5f / root;
		w = (m[k][j] - m[j][k]) * root;
		*tmp[j] = (m[j][i] + m[i][j]) * root;
		*tmp[k] = (m[k][i] + m[i][k]) * root;
	}
}
//void Quat::to_rotation_matrix(mat3& m) const
void Quat::get_rotation_matrix(mat3& m) const
{
	float tx = x + x;
	float ty = y + y;
	float tz = z + z;
	float twx = tx * w;
	float twy = ty * w;
	float twz = tz * w;
	float txx = tx * x;
	float txy = tx * y;
	float txz = tx * z;
	float tyy = ty * y;
	float tyz = ty * z;
	float tzz = tz * z;

	m[0][0] = 1.f - (tyy + tzz);
	m[0][1] = txy - twz;
	m[0][2] = txz + twy;
	m[1][0] = txy + twz;
	m[1][1] = 1.f - (txx + tzz);
	m[1][2] = tyz - twx;
	m[2][0] = txz - twy;
	m[2][1] = tyz + twx;
	m[2][2] = 1.f - (txx + tyy);
}
//void Quat::to_rotation_matrix(mat4& m) const
void Quat::get_rotation_matrix(mat4& m) const
{
	float tx = x + x;
	float ty = y + y;
	float tz = z + z;
	float twx = tx * w;
	float twy = ty * w;
	float twz = tz * w;
	float txx = tx * x;
	float txy = tx * y;
	float txz = tx * z;
	float tyy = ty * y;
	float tyz = ty * z;
	float tzz = tz * z;

	m[0][0] = 1.f - (tyy + tzz);
	m[0][1] = txy - twz;
	m[0][2] = txz + twy;
	m[0][3] = 0.f;
	m[1][0] = txy + twz;
	m[1][1] = 1.f - (txx + tzz);
	m[1][2] = tyz - twx;
	m[1][3] = 0.f;
	m[2][0] = txz - twy;
	m[2][1] = tyz + twx;
	m[2][2] = 1.f - (txx + tyy);
	m[2][3] = 0.f;
	m[3][0] = m[3][1] = m[3][2] = 0.f;
	m[3][3] = 1.f;
}
mat4 Quat::get_rotation_matrix() const
{
	mat4 m;
	get_rotation_matrix(m);

	return m;
}

void Quat::from_angle_axis(float radians, const vec3& axis)
{
	/* Note: axis is assumed unit. The quaternion representing the
	 * rotation is q = cos(A/2) + sin(A/2) * (i*x + j*y + k*z).
	 */
	float half_angle(.5f * radians);
	float sin_half_angle = sin(half_angle);
	w = cos(half_angle);
	x = sin_half_angle * axis.x;
	y = sin_half_angle * axis.y;
	z = sin_half_angle * axis.z;
}
void Quat::to_angle_axis(float& radians, vec3& axis) const
{
	float sqr_len = x * x + y * y + z * z;
	if (sqr_len > 0.f) {
		radians = 2.f * acos(w);
		float inv_len = 1.f / sqrt(sqr_len);
		axis.x = x * inv_len;
		axis.y = y * inv_len;
		axis.z = z * inv_len;
	} else {
		/* Angle is 0 (mod 2pi), so any axis is fine. */
		radians = 0.f;
		axis.x = 1.f;
		axis.y = 0.f;
		axis.z = 0.f;
	}
}

void Quat::from_euler(float yaw, float pitch, float roll, AngleOrder order)
{
	float c1 = cos(pitch * .5f); //c2
	float c2 = cos(yaw * .5f); //c1
	float c3 = cos(roll * .5f);
	float s1 = sin(pitch * .5f); //s2
	float s2 = sin(yaw * .5f); //s1
	float s3 = sin(roll * .5f);

	if (order == ZYX) {
//		w = c1 * c2 * c3 + s1 * s2 * s3;
		w = c2 * c1 * c3 + s2 * s1 * s3;
//		x = s1 * c2 * c3 - c1 * s2 * s3;
		x = s2 * c1 * c3 - c2 * s1 * s3;
//		y = c1 * c3 * s2 + s1 * s3 * c2;
		y = c2 * c3 * s1 + s2 * s3 * c1;
//		z = c1 * c2 * s3 - s1 * s2 * c3;
		z = c2 * c1 * s3 - s2 * s1 * c3;
	} else if (order == XYZ) {
//		w = c1 * c2 * c3 - s1 * s2 * s3;
		w = c2 * c1 * c3 - s2 * s1 * s3;
//		x = s1 * c2 * c3 + c1 * s2 * s3;
		x = s2 * c1 * c3 + c2 * s1 * s3;
//		y = c1 * s2 * c3 - s1 * c2 * s3;
		y = c2 * s1 * c3 - s2 * c1 * s3;
//		z = s1 * s2 * c3 + c1 * c2 * s3;
		z = s2 * s1 * c3 + c2 * c1 * s3;
	} else {
//		w = c1 * c2 * c3 - s1 * s2 * s3;
		w = c2 * c1 * c3 - s2 * s1 * s3;
//		x = s1 * c2 * c3 + c1 * s2 * s3;
		x = s2 * c1 * c3 + c2 * s1 * s3;
//		y = c1 * c3 * s2 + s1 * s3 * c2;
		y = c2 * c3 * s1 + s2 * s3 * c1;
//		z = c1 * s3 * c2 - s1 * s2 * c3;
		z = c2 * s3 * c1 - s2 * s1 * c3;
	}
}

void Quat::from_axes(const vec3 *axis)
{
	mat3 m;
	for (unsigned int i = 0; i < 3; i++) {
		m[0][i] = axis[i].x;
		m[1][i] = axis[i].y;
		m[2][i] = axis[i].z;
	}
	from_rotation_matrix(m);
}
void Quat::from_axes(const vec3& x_axis, const vec3& y_axis, const vec3& z_axis)
{
	mat3 m;
	m[0][0] = x_axis.x;
	m[1][0] = x_axis.y;
	m[2][0] = x_axis.z;

	m[0][1] = y_axis.x;
	m[1][1] = y_axis.y;
	m[2][1] = y_axis.z;

	m[0][2] = z_axis.x;
	m[1][2] = z_axis.y;
	m[2][2] = z_axis.z;

	from_rotation_matrix(m);
}

void Quat::to_axes(vec3 *axis) const
{
	mat3 m;
	get_rotation_matrix(m);
//	to_rotation_matrix(m);
	for (unsigned int i = 0; i < 3; i++) {
		axis[i].x = m[0][i];
		axis[i].y = m[1][i];
		axis[i].z = m[2][i];
	}
}
vec3 Quat::get_x_axis() const
{
	float ty = 2.f * y;
	float tz = 2.f * z;
	float twy = ty * w;
	float twz = tz * w;
	float txy = ty * x;
	float txz = tz * x;
	float tyy = ty * y;
	float tzz = tz * z;

	return vec3(1.f - (tyy + tzz), txy + twz, txz - twy);
}
vec3 Quat::get_y_axis() const
{
	float tx = 2.f * x;
	float ty = 2.f * y;
	float tz = 2.f * z;
	float twx = tx * w;
	float twz = tz * w;
	float txx = tx * x;
	float txy = tx * y;
	float tyz = ty * z;
	float tzz = tz * z;

	return vec3(txy - twz, 1.f - (txx + tzz), tyz + twx);
}
vec3 Quat::get_z_axis() const
{
	float tx = 2.f * x;
	float ty = 2.f * y;
	float tz = 2.f * z;
	float twx = tx * w;
	float twy = ty * w;
	float txx = tx * x;
	float txz = tx * z;
	float tyy = ty * y;
	float tyz = ty * z;

	return vec3(txz + twy, tyz - twx, 1.f - (txx + tyy));
}
void Quat::to_axes(vec3& x_axis, vec3& y_axis, vec3& z_axis) const
{
	mat3 m;
	get_rotation_matrix(m);
//	to_rotation_matrix(m);
	x_axis.x = m[0][0];
	x_axis.y = m[1][0];
	x_axis.z = m[2][0];

	y_axis.x = m[0][1];
	y_axis.y = m[1][1];
	y_axis.z = m[2][1];

	z_axis.x = m[0][2];
	z_axis.y = m[1][2];
	z_axis.z = m[2][2];

}
Quat Quat::operator+(const Quat& q) const
{
	return Quat(w + q.w, x + q.x, y + q.y, z + q.z);
}
Quat Quat::operator-(const Quat& q) const
{
	return Quat(w - q.w, x - q.x, y - q.y, z - q.z);
}
Quat Quat::operator*(const Quat& q) const
{
	return Quat(w * q.w - (x * q.x + y * q.y + z * q.z),
		    w * q.x + x * q.w + y * q.z - z * q.y,
		    w * q.y + y * q.w + z * q.x - x * q.z,
		    w * q.z + z * q.w + x * q.y - y * q.x);
}
Quat Quat::operator*(float scalar) const
{
	return Quat(scalar * w, scalar * x, scalar * y, scalar * z);
}
Quat operator*(float s, const Quat& q)
{
	return Quat(s * q.w, s * q.x, s * q.y, s * q.z);
}
Quat Quat::operator-() const
{
	return Quat(-w, -x, -y, -z);
}
float Quat::dot(const Quat& q) const
{
	return w * q.w + x * q.x + y * q.y + z * q.z;
}
float Quat::length() const
{
	return sqrt(w * w + x * x + y * y + z * z);
}
//float Quat::normal_length() const
float Quat::length_squared() const
{
	return w * w + x * x + y * y + z * z;
}
//float Quat::normalize()
Quat& Quat::normalize()
{
	float len = length();
	*this = *this * (1.f / len);

	return *this;
//	return len;
}
Quat Quat::inverse() const
{
//	float norm = normal_length();
	float norm = length_squared();
	if (norm > 0.f) {
		norm = 1.f / norm;
		return Quat(w * norm, -x * norm, -y * norm, -z * norm);
	}

	return ZERO;
}
Quat Quat::unit_inverse() const
{
	/* Assumed 'this' is kept unit. */
	return Quat(w, -x, -y, -z);
}

Quat Quat::exp() const
{
	/*
	 * If q = A*(x*i + y*j + z*k) where <x,y,z> is unit, then
	 * exp(q) = cos(A) + sin(A) * (x*i + y*j + z*k). If sin(A) is near
	 * zero, use exp(q) = cos(A)+A*(x*i+y*j+z*k) since A/sin(A) has
	 * limit 1.
	 */
	float radians(sqrt(x * x + y * y + z * z));
	float sin_angle = sin(radians);
	Quat q(cos(radians));
	if (fabs(sin_angle) >= EPSILON) {
		float coef = sin_angle / radians;
		q.x = coef * x;
		q.y = coef * y;
		q.z = coef * z;
	} else {
		q.x = x;
		q.y = y;
		q.z = z;
	}

	return q;
}
Quat Quat::log() const
{
	Quat q(0.f);
	if (fabs(w) < 1.f) {
		float radians(acos(w));
		float sin_angle = sin(radians);
		if (fabs(sin_angle) >= EPSILON) {
			float coef = radians / sin_angle;
			q.x = coef * x;
			q.y = coef * y;
			q.z = coef * z;

			return q;
		}
	}
	q.x = x;
	q.y = y;
	q.z = z;

	return q;
}
vec3 Quat::operator*(const vec3& v) const
{
	/* nVidia SDK implementation */
	vec3 qvec(x, y, z);
	vec3 uv(cross(qvec, v));
	vec3 uuv(cross(qvec, uv));
	uv *= 2.f * w;
	uuv *= 2.f;

	return v + uv + uuv;
}

vec4 Quat::operator*(const vec4& v) const
{
	return vec4(*this * vec3(v.x, v.y, v.z), v.w);
}

Quat Quat::slerp(float ft, const Quat& q1, const Quat& q2, int shortest_path)
{
	float fcos = q1.dot(q2);
	Quat q;

	/* Do we need to invert the rotation? */
	if (fcos < 0.f && shortest_path) {
		fcos = -fcos;
		q = -q;
	} else {
		q = q2;
	}

	/* Standard case */
	if (fabs(fcos) < (1.f - EPSILON)) {
		float fsin = sqrt(1.f - (fcos * fcos));
		float fangle = atan2(fsin, fcos);
		float inv_sin = 1.f / fsin;
		float coef1 = sin((1.f - ft) * fangle) * inv_sin;
		float coef2 = sin(ft * fangle) * inv_sin;

		return coef1 * q1 + coef2 * q;
	}

	/* Two situations:
	 * 1. q1 and q2 are very close (fcos ~= 1), so can safely do
	 *    linear interpolation.
	 * 2. q1 and q2 are almost inverse (fcos ~= -1), there are
	 *    infinite possibilities for interpolation, so just use
	 *    linear.
	 */
	Quat t = (1.f - ft) * q1 + ft * q;
	return t.normalize();
//	t.normalize();
//	return t;
}
Quat Quat::slerp_extra(float ft, const Quat& q1, const Quat& q2, int n_extra)
{
	float fcos = q1.dot(q2);
	float fangle(acos(fcos));
	if (fabs(fangle) < EPSILON)
		return q1;
	float fsin = sin(fangle);
	float fphase(M_PI * n_extra * ft);
	float inv_sin = 1.f / fsin;
	float coef1 = sin((1.f - ft) * fangle - fphase) * inv_sin;
	float coef2 = sin(ft * fangle + fphase) * inv_sin;
	return coef1 * q1 + coef2 * q2;
}

/* q1, q2, q3 must be unit */
void Quat::intermediate(const Quat& q1, const Quat& q2, const Quat& q3,
							Quat& qa, Quat& qb)
{
	Quat q1_inv = q1.unit_inverse();
	Quat q2_inv = q2.unit_inverse();
	Quat p1 = q1_inv * q2;
	Quat p2 = q2_inv * q3;
	Quat arg = .25f * (p1.log() - p2.log());
	Quat neg_arg = -arg;
	qa = q2 * arg.exp();
	qb = q2 * neg_arg.exp();
}
Quat Quat::squad(float ft, const Quat& q1, const Quat& q2, const Quat& q3,
		const Quat& q4, int shortest_path)
{
	float slerp_t = 2.f * ft * (1.f - ft);
	Quat slerp1 = this->slerp(ft, q1, q4, shortest_path);
	Quat slerp2 = this->slerp(ft, q2, q3);
	return slerp(slerp_t, slerp1, slerp2);
}
float Quat::get_roll(int reproject_axis) const
{
	if (!reproject_axis)
		return atan2(2.f * (w*y + w*z), w*w + x*x - y*y - z*z);

	float ty = 2.f * y;
	float tz = 2.f * z;
	float twz = tz * w;
	float txy = ty * x;
	float tyy = ty * y;
	float tzz = tz * z;
	return atan2(txy + twz, 1.f - (tyy + tzz));
}
float Quat::get_pitch(int reproject_axis) const
{
	if (!reproject_axis)
		return atan2(2.f * (y*z + w*x), w*w - x*x - y*y + z*z);

	float tx = 2.f * x;
	float tz = 2.f * z;
	float twx = tx * w;
	float txx = tx * x;
	float tyz = tz * y;
	float tzz = tz * z;
	return atan2(tyz + twx, 1.f - (txx + tzz));
}
float Quat::get_yaw(int reproject_axis) const
{
	if (!reproject_axis)
		return asin(-2.f * (x*z - w*y));
	float tx = 2.f * x;
	float ty = 2.f * y;
	float tz = 2.f * z;
	float twy = ty * w;
	float txx = tx * x;
	float txz = tx * z;
	float tyy = ty * y;
	return atan2(txz + twy, 1.f - (txx + tyy));
}
Quat Quat::nlerp(float ft, const Quat& q1, const Quat& q2, int shortest_path)
{
	Quat q;
	float fcos = q1.dot(q2);
	if (fcos < 0.f && shortest_path)
		q = q1 + ft * ((-q2) - q1);
	else
		q = q1 + ft * (q2 - q1);
	return q.normalize();
//	q.normalize();
//	return q;
}

} /* namespace dengine */
