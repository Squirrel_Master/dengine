#include <graphics/texture.h>

namespace dengine {

Texture::Texture() : _id(0),
		     _offset(0),
		     _bound(0),
		     _width(0.f),
		     _height(0.f),
		     _depth(0.f)
{
	glGenTextures(1, &_id);
}

Texture::~Texture()
{
	glDeleteTextures(1, &_id);
}

} /* namespace dengine */
