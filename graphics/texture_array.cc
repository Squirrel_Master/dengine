#include <graphics/texture_array.h>

namespace dengine {

TextureArray::TextureArray(vector<ImageData *>& images,
			   int max_w,
			   int max_h,
			   int layout_offset)
{
	init_images(images, max_w, max_h, layout_offset);
}

TextureArray::TextureArray(vector<Font>& fonts,
			   vector<FT_Face>& faces,
			   int layout_offset)
{
	init_fonts(fonts, faces, layout_offset);
}

/*
 * init_images:
 * Takes a vector of ImageData and builds an array textures out of them. Each
 * layer of depth is essentially a 2D texture build from one of the images.
 */
int TextureArray::init_images(vector<ImageData *>& images,
			      int max_w,
			      int max_h,
			      int layout_offset)
{
	if (layout_offset < 0) {
		int max;
		glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &max);
		fprintf(stderr, "Texture layout must be between 0 and %d\n",
									max);
		return -1;
	}

	if (images.size() < 1) {
		fprintf(stderr, "No images to send to Texture2D_Array\n");
		return -1;
	}

	int bpp = images[0]->get_bpp();

	/* Iterate through the images and find the maximum dimensions. */
	if (max_w < 1 || max_h < 1) {
		for (int i = 0; i < images.size(); i++) {
			int w = images[i]->get_width();
			int h = images[i]->get_height();
			if (w < 1 || h < 1 || images[i]->get_bpp() != bpp)
				return -1;
			if (w > max_w)
				max_w = w;
			if (h > max_h)
				max_h = h;
		}
	}
	_width = max_w;
	_height = max_h;
	_depth = images.size();

	_offset = layout_offset;
	glActiveTexture(GL_TEXTURE0 + _offset);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _id);

	GLenum in_format, out_format, out_type;

	/*
	 * Set the internalFormat (in_format), format of pixel data
	 * (out_format), and the data type of the pixel data based on bits per
	 * pixel. These settings seem to be the most common.
	 */
	switch (bpp) {
	case 8:
		in_format = GL_R3_G3_B2;
		out_format = GL_RGB;
		out_type = GL_UNSIGNED_BYTE_3_3_2;
		break;
	case 16:
		in_format = GL_RGB5;
		out_format = GL_RGB;
		out_type = GL_UNSIGNED_SHORT_5_6_5;
		break;
	case 24:
		in_format = GL_RGB;
		out_format = GL_BGR;
		out_type = GL_UNSIGNED_BYTE;
		break;
	default:
		in_format = GL_RGBA;
		out_format = GL_BGRA;
		out_type = GL_UNSIGNED_BYTE;
		break;
	}

	/* Create the 3D texture. */
	glTexImage3D(GL_TEXTURE_2D_ARRAY, /* GLenum target */
		     0,			  /* GLint level */
		     in_format,		  /* GLint internalFormat */
		     _width,		  /* GLsizei width */
		     _height,		  /* GLsizei height */
		     _depth,		  /* GLsizei depth */
		     0,			  /* GLint border */
		     out_format,	  /* GLenum format */
		     out_type,		  /* GLenum type */
		     NULL);		  /* const GLvoid *data */

	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);


	/* Send the image data to the 3D texture. */
	for (int i = 0; i < images.size(); i++) {
		glTexSubImage3D(GL_TEXTURE_2D_ARRAY,
				0,
				0,
				0,
				i,
				images[i]->get_width(),
				images[i]->get_height(),
				1,
				out_format,
				out_type,
				images[i]->get_pixels());
	}

/*
 * Note: Consider how to create mipmaps...
 * glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_GENERATE_MIPMAP, GL_TRUE);
 * glGenerateMipmap(GL_TEXTURE_2D_ARRAY); // perhaps use instead of above
 */
	glBindTexture(GL_TEXTURE_2D_ARRAY, 0);

	printf("Generated a %.2f X %.2f X %d (%.2f kB) texture array\n",
				_width,
				_height,
				images.size(),
				_width * _height * images.size() / 1024.f);
	return 0;
}

/*
 * init_fonts:
 * Builds an array texture using an input vector of fonts and their faces
 * and accounts for the case in which corresponding face data has been
 * deleted. Like init_images above, each layer of depth is essentially
 * a 2D texture of a font. However, each of these textures is also a
 * texture atlas, wherein each glyph of the font has been pasted onto
 * the 2D texture (Think of this as an array of texture atlases).
 *
 * Note: This function has been specialized to work directly with the
 * fontmanager, which saves font data and face data in its vectors _fonts
 * and _faces, respectively.  Once the fontmanager calls build_texture_array,
 * this function is called with its CURRENT _fonts and _faces. Immediately
 * afterwards, all the FreeType resources are released (including the data
 * in _faces). Thus, if new fonts are added to the manager, there will be
 * an offset between the current fonts and the new faces, which is accounted
 * for here.  Bear in mind, each additional sampler in the shader decreases
 * performance, so its best to keep fonts together, if possible.
 */
int TextureArray::init_fonts(vector<Font>& fonts,
			     vector<FT_Face>& faces,
			     int layout_offset)
{
	if (layout_offset < 0 || fonts.size() < 1)
		return -1;

	/* Ensure proper correspondence between fonts and faces. */
	int font_offset = fonts.size() - faces.size();

	if (font_offset < 0)
		return -1;

	/* Determine the dimensions of the texture array. */
	for (int i = 0; i < faces.size(); i++) {
		fonts[i + font_offset]._texture = this;
		fonts[i + font_offset]._idx = i;

		FT_GlyphSlot g = faces[i]->glyph;
		int curr_w = 0, curr_h = 0, row_w = 0, row_h = 0;

		/* Compare the sizes of each character in the face. */
		for (int j = 32; j < 128; j++) {
			if (FT_Load_Char(faces[i], j, FT_LOAD_RENDER)) {
				fprintf(stderr, "Unable to load %c.\n", j);
				continue;
			}

			if (row_w + g->bitmap.width + 1 >= MAX_WIDTH) {
				if (curr_w  < row_w)
					curr_w = row_w;
				curr_h += row_h;
				row_w = row_h = 0;
			}

			row_w += g->bitmap.width + 1;
			if (row_h < g->bitmap.rows)
				row_h = g->bitmap.rows;
		}

		if (curr_w < row_w)
			curr_w = row_w;

		if (_width < curr_w)
			_width = curr_w;

		if (_height < (curr_h += row_h))
			_height = curr_h;
	}

	/* Create a texture to hold all glyphs of each font. */
	_offset = layout_offset;

	glActiveTexture(GL_TEXTURE0 + _offset);
	glBindTexture(GL_TEXTURE_2D_ARRAY, _id);

	glTexImage3D(GL_TEXTURE_2D_ARRAY,  /* GLenum target */
		     0,			   /* GLint level */
		     GL_RED,		   /* GLint internalFormat */
		     _width,		   /* GLsizei width */
		     _height,		   /* GLsizei height */
		     faces.size(),	   /* GLsizei depth */
		     0,			   /* GLint border */
		     GL_RED,		   /* GLenum format */
		     GL_UNSIGNED_BYTE,	   /* GLenum type */
		     NULL);		   /* const GLvoid *data */

	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/* Ensure 1 byte alignment when uploading the texture data. */
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	/*
	 * Paste all the glyph bitmaps of each face onto the corresponding
	 * texture layer.
	 */
	for (int i = 0; i < faces.size(); i++) {
		FT_GlyphSlot g = faces[i]->glyph;
		int offset_x = 0, offset_y = 0, row_h = 0;

		for (int j = 32; j < 128; j++) {
			if (FT_Load_Char(faces[i], j, FT_LOAD_RENDER)) {
				fprintf(stderr, "Unable to load %c.\n", j);
				continue;
			}

			if (offset_x + g->bitmap.width + 1 >= MAX_WIDTH) {
				offset_y += row_h;
				row_h = 0;
				offset_x = 0;
			}

			glTexSubImage3D(GL_TEXTURE_2D_ARRAY,
					0,
					offset_x,
					offset_y,
					i,
					g->bitmap.width,
					g->bitmap.rows,
					1,
					GL_RED,
					GL_UNSIGNED_BYTE,
					g->bitmap.buffer);

			int idx = i + font_offset;
			fonts[idx].c[j].ax = g->advance.x >> 6;
			fonts[idx].c[j].ay = g->advance.y >> 6;

			fonts[idx].c[j].bw = g->bitmap.width;
			fonts[idx].c[j].bh = g->bitmap.rows;

			fonts[idx].c[j].bl = g->bitmap_left;
			fonts[idx].c[j].bt = g->bitmap_top;

			fonts[idx].c[j].tx = offset_x / (float)_width;
			fonts[idx].c[j].ty = offset_y / (float)_height;

			if (row_h < g->bitmap.rows)
				row_h = g->bitmap.rows;
			offset_x += g->bitmap.width + 1;
		}
	}

/*
 * Note: Consider how to create mipmaps...
 * glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_GENERATE_MIPMAP, GL_TRUE);
 * glGenerateMipmap(GL_TEXTURE_2D_ARRAY); // perhaps use instead of above
 */
	glBindTexture(GL_TEXTURE_2D_ARRAY, 0);

	printf("Generated a %.2f X %.2f X %d (%.2f kB) texture array\n",
				_width,
				_height,
				faces.size(),
				_width * _height * faces.size() / 1024.f);

	return 0;
}

void TextureArray::bind()
{
	if (!_bound) {
		glActiveTexture(GL_TEXTURE0 + _offset);
		glBindTexture(GL_TEXTURE_2D_ARRAY, _id);
		_bound = 1;
	}
}

void TextureArray::unbind()
{
	if (_bound) {
		glActiveTexture(GL_TEXTURE0 + _offset);
		glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
		_bound = 0;
	}
}

} /* namespace dengine */
