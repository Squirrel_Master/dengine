#include <graphics/camera.h>

namespace dengine {

Camera::Camera() : _view(1.f),
		   _proj(1.f),
		   _vp_matrix(1.f),
		   _rotation(1.f),
		   _pos(0.f, 0.f, 0.f),
		   _translation(translation(_pos)),
		   _shader(NULL),
		   _moved(0),
		   _rotated(0),
		   _proj_changed(0),
		   _view_changed(0),
		   _speed(1.f),
		   _sensitivity(1.f),
		   _vpid(-1),
		   _aw(16.f),
		   _ah(9.f),
		   _aw_scaled(16.f),
		   _ah_scaled(9.f),
		   _znear(.01f),
		   _zfar(1.f),
		   _fov(45.f),
		   _fov_scaled(45.f),
		   _ptype(PERSPECTIVE),
		   _cam_type(FREE)
{
}

void Camera::set_projection(float aspect_width,
			    float aspect_height,
			    float znear,
			    float zfar,
			    float fov,
			    ProjectionType pt)
{
	/* Set aspect parameters (default to 16:9). */
	_aw = aspect_width > 0.f ? aspect_width : 16.f;
	_ah = aspect_height > 0.f ? aspect_height : 9.f;
	_aw_scaled = _aw;
	_ah_scaled = _ah;

	/* Set z clipping distances (default [-1, 1]). */
	if (znear > zfar) {
		_znear = zfar;
		_zfar = _znear;
	} else if (znear == zfar) {
		_znear = .01f;
		_zfar = 1.f;
	} else {
		_znear = znear;
		_zfar = zfar;
	}

	/* Set vertical field of view, in degrees (default to 45). */
	_fov = fov > 0.f ? fov : 45.f;
	_fov_scaled = _fov;

	_ptype = pt;

	update_projection();
}

void Camera::set_shader(Shader *s, const char *uname)
{
	if (!s)
		return;
	_shader = s;
	_shader->enable();

	if (uname)
		_vpid = s->set_uniform_mat4(uname, _vp_matrix, TRUE);
	else
		_vpid = s->set_uniform_mat4("VP_matrix", _vp_matrix, TRUE);
}

/*
 * move(MoveDirection):
 * Moves the camera as dictated by the input direction. Bear in mind, strict
 * usage of this function to simulate diagonal movement will result in an
 * increased rate of movement since the resulting movement would be along
 * the hypotenuse of the right triangle produced by the two directions.
 */
void Camera::move(MoveDirection dir)
{
	if (dir == UP) {
		_pos += normalize(_orientation.conjugate() * vec3(0, 1, 0)) *
									_speed;
	} else if (dir == DOWN) {
		_pos -= normalize(_orientation.conjugate() * vec3(0, 1, 0)) *
									_speed;
	} else if (dir == LEFT) {
		_pos += normalize(_orientation.conjugate() * vec3(-1, 0, 0)) *
									_speed;
	} else if (dir == RIGHT) {
		_pos -= normalize(_orientation.conjugate() * vec3(-1, 0, 0)) *
									_speed;
	} else if (dir == FRONT) {
		_pos += normalize(_orientation.conjugate() * vec3(0, 0, -1)) *
									_speed;
	} else if (dir == BACK) {
		_pos -= normalize(_orientation.conjugate() * vec3(0, 0, -1)) *
									_speed;
	} else {
		return;
	}

	_moved = 1;
}

/*
 * move(vec4):
 * Allows for more fine-tuned movement of the camera's position, as would be
 * necessary for joystick use. Also, use of this function is required to
 * normalize movement speed along diagonals.
 */
void Camera::move(const vec3& unit_dir)
{
	_pos += normalize(_orientation.conjugate() * unit_dir) * _speed;
	_moved = 1;
}

/*
 * rotate:
 * Handles rotations according to the type of the camera, FREE or FPS.
 * Bear in mind, no attempt to limit rotations is provided (for
 * instance, if for an FPS style camera the user wants to lock the pitch
 * to +/-90 degrees, this should be handled prior to sending the angle).
 *
 * Note: Angles are assumed to be in radians!
 */
void Camera::rotate(float rad_pitch,
		    float rad_yaw,
		    float rad_roll,
		    AngleOrder order)
{
	/* Flight simulator style camera. */
	if (_cam_type == FREE) {
		Quat rot(rad_pitch, rad_yaw, rad_roll, order);
		_orientation = rot * _orientation;
		_orientation.normalize();

	/* First person shooter style camera (no rotation about Z).*/
	} else if (_cam_type == FPS) {

		/*
		 * For FPS, be sure to isolate the rotations and apply
		 * them in opposite orders so as to negate rolling about
		 * the z-axis (hence why _orientation is between the two
		 * rotations in the concatenation).
		 */
		Quat pitch_rot(rad_pitch, vec3(1.f, 0.f, 0.f));
		_orientation = pitch_rot * _orientation;

		/* If the camera is inverted in y, flip the yaw rotation. */
		if (_orientation.get_y_axis().y < 0.f)
			_orientation = _orientation * Quat(rad_yaw, vec3(0.f, -1.f, 0.f));
		else
			_orientation = _orientation * Quat(rad_yaw, vec3(0.f, 1.f, 0.f));

		_orientation.normalize();
	}

	_rotated = 1;
}

/*
 * zoom:
 * Zooming is simulated by making adjustments to the camera's field of view.
 */
void Camera::zoom(float zoom_factor)
{
	if (_ptype == PERSPECTIVE) {
		_fov_scaled += _fov * zoom_factor;
		if (_fov_scaled < .00001f)
			_fov_scaled = .00001f;
		else if (_fov_scaled > 180.f)
			_fov_scaled = 180.f;
	} else {
		_aw_scaled += _aw * zoom_factor;
		_ah_scaled += _ah * zoom_factor;
	}

	update_projection();
}

void Camera::reset_zoom()
{
	_fov_scaled = _fov;
	_aw_scaled = _aw;
	_ah_scaled = _ah;
	update_projection();
}

void Camera::set_orientation(const Quat& q)
{
	_orientation = q;
	_orientation.normalize();

	_rotated = 1;
}

void Camera::set_position(const vec3& v)
{
	_pos = v;
	_moved = 1;
}

void Camera::update_projection()
{
	/* Build projection matrix. */
	if (_ptype == ORTHOGRAPHIC)
		_proj = ortho(-_aw_scaled, _aw_scaled, -_ah_scaled, _ah_scaled,
								_znear, _zfar);
	else if (_ptype == PERSPECTIVE)
		_proj = perspective(_fov_scaled, _aw / _ah, _znear, _zfar);

	_proj_changed = 1;
}

/*
 * update_view:
 * Updates _rotation and/or _translation if a rotation or translation has
 * occurred and then calculates the new view matrix. Finally, the flag
 * _view_changed is set so that the _vp_matrix will be updated accordingly.
 */
void Camera::update_view()
{
	if (_rotated) {
		_orientation.get_rotation_matrix(_rotation);
		_rotated = 0;
	}
	if (_moved) {
		_translation = translation(-_pos);
		_moved = 0;
	}

	_view = _rotation * _translation;
	_view_changed = 1;
}

/*
 * update:
 * Updates the various camera matrices based on the flags _rotated, _moved,
 * _proj_changed, and _view_changed.  If _proj or _view have changed, then
 * _vp_matrix is updated and sent to the shader.
 */
void Camera::update()
{
	if (_rotated || _moved)
		update_view();

	if (_proj_changed || _view_changed) {
		_vp_matrix = _proj * _view;
		if (_shader && _vpid >= 0) {
			_shader->enable();
			_shader->set_uniform_mat4(_vpid, _vp_matrix, GL_TRUE);
		}
		_proj_changed = 0;
		_view_changed = 0;
	}
}

} /* namespace dengine */
