#ifndef _VERTEX_H_
#define _VERTEX_H_

#include <math/vec.h>

namespace dengine {

/*
 * pack_color:
 * Packs color values into 4 bytes.
 * NOTE: r, g, b, a are assumed to be in the interval [0, 1].
 */
static unsigned int pack_color(float r, float g, float b, float a)
{
	if (r < 0.f || g < 0.f || b < 0.f || a < 0.f) {
		fprintf(stderr, "Color coordinates must be greater than 0.\n");
		return 0;
	}

	return (((int)(a * 255.f)) << 24) |
	       (((int)(b * 255.f)) << 16) |
	       (((int)(g * 255.f)) << 8) |
	       ((int)(r * 255.f));
}

typedef struct vertex2 {
	vec2 pos;
	vec2 uv;
	float t_id;
	float t_idx;
	unsigned int color;
} Vertex2;

typedef struct vertex3 {
	vec3 pos;
	vec2 uv;
	float t_id;
	float t_idx;
	unsigned int color;
} Vertex3;

typedef struct vertex4 {
	vec4 pos;
	vec2 uv;
	float t_id;
	float t_idx;
        unsigned int color;
} Vertex4;

typedef struct mesh_vertex {
	vec3 pos;
	vec3 normal;
	vec3 tangent;
	vec2 uv;
	float t_id;
	float t_idx;
	unsigned int color;
} MeshVertex;

} /* namespace dengine */

#endif /* _VERTEX_H_ */
