#ifndef _IO_H_
#define _IO_H_

#include <vector>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <FreeImage.h>
#include <math/vec.h>

#define BUF_SIZE 4096

using std::vector;

namespace dengine {

//int read_wavefront(const char *filename, vector<unsigned int> &tri_ids,
//						vector<vec3> &tri_verts);
int read_wavefront(const char *filename,
		   vector<vec3> *vertices,
		   vector<unsigned int> *indices,
		   vector<vec3> *normals = NULL,
		   vector<vec3> *tangents = NULL,
		   vector<vec2> *uv = NULL);

int read_file(const char *filename, std::vector<unsigned char> &buffer);
char *read_file(const char *filename);

class ImageData {
public:
	ImageData();
	ImageData(const char *filename, int format = 32);
	~ImageData();

	int load(const char *filename, int format = 32);
	void unload();
	inline const unsigned char *get_pixels() const { return _pixels; }
	inline const int get_width() const { return _width; }
	inline const int get_height() const { return _height; }
	inline const int get_bpp() const { return _bpp; }

private:
	int _width, _height, _bpp;
	FIBITMAP *_dib;
	unsigned char *_pixels;
};

} /* namespace dengine */

#endif /* _IO_H_ */
