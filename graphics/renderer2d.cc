#include <graphics/renderer2d.h>

namespace dengine {

Renderer2D::Renderer2D() : _rbuf(NULL)
{
	_transform_stack.push_back(mat4(1.f));
}

Renderer2D::~Renderer2D()
{
        destroy();
}

void Renderer2D::destroy()
{
	/*
	 * Even though the renderables and the shaders are not created within
	 * the renderer, current usage dictates that renderables and shaders
	 * belong to one and only one renderer.  Moving forward, reference
	 * counting or other such smart pointers may be used, in the event
	 * the ability to share renderables and shaders amongst renderers
	 * is sensible.
	 */
	_root.destroy();

	if (_shader) {
		delete _shader;
		_shader = NULL;
	}

	if (_rbuf) {
		delete _rbuf;
		_rbuf = NULL;
	}
}

/*
 * add:
 * Insert a batch or parent batch and child batch (if child is not NULL) to
 * the renderer's batch hierarchy (_root).  Using this function to build the
 * renderer's batch hierarchy, rather than directly adding batches to other
 * batches, prevents cycles and copying of batches.
 */
void Renderer2D::add(Batch<Renderable2D> *parent, Batch<Renderable2D> *child)
{
	Batch<Renderable2D> *tmp;

	/* Check if the parent is already in the tree. */
	if (tmp = _root.find(parent)) {

		/* If the child is not already in the tree, insert it. */
		if (child && !(_root.find(child)))
			tmp->add(child);
		return;
	}

	/* The parent is not in the tree, thus check the child. */
	if (child && !(tmp = _root.find(child)))
		parent->add(child);

	_root.add(parent);
}

/*
 * remove:
 * Removes the batch from the renderer's batch hierarchy (_root).
 */
void Renderer2D::remove(Batch<Renderable2D> *batch)
{
	if (batch)
		_root.remove(batch);
}

/*
 * add_shader:
 * Sets the shader.  Currently, the use of only one shader is supported.
 */
//void Renderer2D::add_shader(Shader *s)
//{
//	if (s)
//		_shader = s;
//}

/*
 * render:
 * Traverses the batch tree (_root) and draws renderables in depth-first
 * order, propagating transformations downwards.
 */
void Renderer2D::render()
{
	if (!_shader) {
		fprintf(stderr, "Missing shader, cannot render\n");
		return;
	}

	_shader->enable();

	_rbuf->reset();
	_rbuf->begin();

	submit(&_root);

        _rbuf->end();
        _rbuf->flush();
}

/*
 * submit:
 * Submits renderables to the shader in depth first order, multiplying
 * batch transformations on each descent.  The renderables' submit
 * function dictates how to actually send data to the "render buffer"
 * renderbuf.
 */
void Renderer2D::submit(Batch<Renderable2D> *batch)
{
	/* Apply the parent's tranformation to the child batch. */
	_transform_stack.push_back(_transform_stack.back() * batch->_transform);

	/* Recursively submit each batch (applies the transformations down). */
	for (int i = 0; i < batch->_children.size(); i++)
		submit(batch->_children[i]);

	mat4 *t = &_transform_stack.back();

	/* Submit the batch's renderables and the current transformation. */
	for (int i = 0; i < batch->_renderables.size(); i++)
		batch->_renderables[i]->submit(_rbuf, t);

	/* Pop the latest transform (that of the batch and its parent). */
	_transform_stack.pop_back();
}

/*
 * init:
 * Initializes the renderbuf.
 */
void Renderer2D::init()
{
	if (_rbuf) {
		delete _rbuf;
		_rbuf = NULL;
	}

	_rbuf = new Renderbuf2D();
	_rbuf->init();
}

} /* namespace dengine */
