#ifndef _RENDERER2D_H_
#define _RENDERER2D_H_

#include <vector>

#include <math/dmath.h>
#include <graphics/renderer.h>
#include <graphics/renderbuf2d.h>
#include <graphics/batch.h>
#include <graphics/renderable.h>

namespace dengine {

/*
 * class Renderer2d:
 * Renderer2d implements Renderer with the intention of rendering Renderable2Ds
 * only.  By using Renderbuf2D and limiting its use to Renderable2Ds,
 * Renderer2D is faster than Renderer3D at rendering large numbers of
 * Renderable2Ds. For this reason, Renderer2D should be used for GUIs and
 * strict 2D rendering (i.e., 2D games).
 */
class Renderer2D : public Renderer<Renderable2D> {
public:
        Renderer2D();
        ~Renderer2D();

	/* init(): initializes the renderer. */
        void init();

	/* destroy(): deletes all allocated variables. */
        void destroy();

	void add(Batch<Renderable2D> *parent,
					Batch<Renderable2D> *child = NULL);
	void remove(Batch<Renderable2D> *batch);
        void render();

protected:
	void submit(Batch<Renderable2D> *batch);

private:
	vector<mat4> _transform_stack;
	int _draw_calls;
	Renderbuf2D *_rbuf;
};

} /* namespace dengine */

#endif /* _RENDERER2D_H_ */
