#include <graphics/mesh.h>

namespace dengine {

Mesh::Mesh(const Mesh *m)
{
	if (init(m) < 0) {
		fprintf(stderr, "Failed to construct mesh.\n");
		destroy();
	}
}

Mesh::Mesh(const char *filename,
	   const vec4& color,
	   Texture *texture,
	   const int texture_array_index)
{
	if (init(filename, color, texture, texture_array_index) < 0) {
		fprintf(stderr, "Failed to construct mesh %s.\n", filename);
		destroy();
	}
}

Mesh::Mesh(const vector<vec3> *vertices,
	   const vector<unsigned int> *indices,
	   const vec4& color,
	   const vector<vec3> *normals,
	   const vector<vec3> *tangents,
	   const vector<vec2> *uv,
	   Texture *texture,
	   const int texture_array_index) : Renderable3D()
{
	if (init(vertices, indices, color, normals, tangents, uv, texture,
						texture_array_index) < 0) {
		fprintf(stderr, "Failed to construct mesh.\n");
		destroy();
	}
}

Mesh::~Mesh()
{
	destroy();
}

int Mesh::init(const Mesh *m)
{
	if (!m || !m->_vertices || !m->_indices)
		return -1;

	/* Copy color. */
	_color = m->_color;

	/* Copy vertices. */
	_num_vertices = m->_num_vertices;
	_vertices = (vec3 *)malloc(sizeof(vec3) * _num_vertices);
	if (!_vertices) {
		fprintf(stderr, "malloc() failed.\n");
		return -1;
	}
	for (int i = 0; i < _num_vertices; i++)
		_vertices[i] = m->_vertices[i];

	/* Copy indices. */
	_num_indices = m->_num_indices;
	_indices = (unsigned int *)malloc(sizeof(unsigned int) * _num_indices);
	if (!_indices) {
		fprintf(stderr, "malloc() failed.\n");
		return -1;
	}
	for (int i = 0; i < _num_indices; i++)
		_indices[i] = m->_indices[i];

	/* Copy normals. */
	if (m->_normals) {
		_normals = (vec3 *)malloc(sizeof(vec3) * _num_vertices);
		if (!_normals) {
			fprintf(stderr, "malloc() failed.\n");
			return -1;
		}
		for (int i = 0; i < _num_vertices; i++)
			_normals[i] = m->_normals[i];
	}

	/* Copy tangents. */
	if (m->_tangents) {
		_tangents = (vec3 *)malloc(sizeof(vec3) * _num_vertices);
		if (!_tangents) {
			fprintf(stderr, "malloc() failed.\n");
			return -1;
		}
		for (int i = 0; i < _num_vertices; i++)
			_tangents[i] = m->_tangents[i];
	}

	/* Copy texture coordinates (uv). */
	if (m->_uv) {
		_uv = (vec2 *)malloc(sizeof(vec2) * _num_vertices);
		if (!_uv) {
			fprintf(stderr, "malloc() failed.\n");
			return -1;
		}
		for (int i = 0; i < _num_vertices; i++)
			_uv[i] = m->_uv[i];
	}

	/* Copy texture pointer (although why have uv coordinates without?). */
	if (m->_texture) {
		_texture = m->_texture;
		_idx = m->_idx;
	}

	return 0;
}

int Mesh::init(const char *filename,
	       const vec4& color,
	       Texture *texture,
	       const int texture_array_index)
{
	vector<unsigned int> faces;
	vector<vec3> vertices;
	vector<vec3> normals;
	vector<vec3> tangents;
	vector<vec2> uv;
	if (read_wavefront(filename,
			   &vertices,
			   &faces,
			   &normals,
			   &tangents,
			   &uv) < 0) {
		fprintf(stderr, "Unable to parse %s.\n", filename);
		return -1;
	}
	return init(&vertices,
		    &faces,
		    color,
		    &normals,
		    &tangents,
		    &uv,
		    texture,
		    texture_array_index);
}

int Mesh::init(const vector<vec3> *vertices,
	       const vector<unsigned int> *indices,
	       const vec4& color,
	       const vector<vec3> *normals,
	       const vector<vec3> *tangents,
	       const vector<vec2> *uv,
	       Texture *texture,
	       const int texture_array_index)
{
	if (!vertices || !indices)
		return -1;
	destroy();

	_num_indices = indices->size();

	/* The indices must represent triangles (i.e., 3 vertices). */
	if (_num_indices % 3 != 0) {
		_num_indices = 0;
		return -1;
	}

	_num_vertices = vertices->size();

	/* Copy the vertices. */
	_vertices = (vec3 *)malloc(sizeof(vec3) * _num_vertices);
	if (!_vertices) {
		fprintf(stderr, "malloc failed.\n");
		destroy();
		return -1;
	}

	for (unsigned int i = 0; i < _num_vertices; i++)
		_vertices[i] = (*vertices)[i];

	/* Copy texture coordinates. */
	if (uv && (uv->size() == _num_vertices)) {
		_uv = (vec2 *)malloc(sizeof(vec2) * _num_vertices);
		if (!_uv) {
			fprintf(stderr, "malloc failed.\n");
			destroy();
			return -1;
		}
		for (unsigned int i = 0; i < _num_vertices; i++)
			_uv[i] = (*uv)[i];
	}

	/* Copy the indices. */
	_indices = (unsigned int *)malloc(sizeof(unsigned int) * _num_indices);
	if (!_indices) {
		fprintf(stderr, "malloc failed.\n");
		destroy();
		return -1;
	}

	for (unsigned int i = 0; i < _num_indices; i++)
		_indices[i] = (*indices)[i];

	/* Copy vertex normals. */
	if (normals && (normals->size() == _num_vertices)) {
		_normals = (vec3 *)malloc(sizeof(vec3) * _num_vertices);
		if (!_normals) {
			fprintf(stderr, "malloc failed.\n");
			destroy();
			return -1;
		}

		for (unsigned int i = 0; i < _num_vertices; i++)
			_normals[i] = (*normals)[i];
	}

	/* Copy vertex tangents. */
	if (tangents && (tangents->size() == _num_vertices)) {
		_tangents = (vec3 *)malloc(sizeof(vec3) * _num_vertices);
		if (!_tangents) {
			fprintf(stderr, "malloc failed.\n");
			destroy();
			return -1;
		}
		for (unsigned int i = 0; i < _num_vertices; i++)
			_tangents[i] = (*tangents)[i];
	}

	if (!_normals || !_tangents)
		compute_bases();

	_texture = texture;

	_idx = texture_array_index;

	_color = pack_color(color.x, color.y, color.z, color.w);

	return 0;
}

void Mesh::destroy()
{
	if (_vertices) {
		free(_vertices);
		_vertices = NULL;
	}

	if (_normals) {
		free(_normals);
		_normals = NULL;
	}

	if (_tangents) {
		free(_tangents);
		_tangents = NULL;
	}

	if (_uv) {
		free(_uv);
		_uv = NULL;
	}

	if (_indices) {
		free(_indices);
		_indices = NULL;
	}

	_texture = NULL;
	_idx = -1;
	_color = 0;
	_num_vertices = 0;
	_num_indices = 0;
}

void Mesh::transform(const mat4 *t)
{
	if (t) {
		for (int i = 0; i < _num_vertices; i++) {
			_vertices[i] = *t * _vertices[i];
			_normals[i] = *t * _normals[i];
		}
	}
}

void Mesh::transform(mat4 t)
{
	for (int i = 0; i < _num_vertices; i++) {
		_vertices[i] = t * _vertices[i];
		_normals[i] = t * _normals[i];
	}
}

void Mesh::submit(Renderbuf3D *rbuf, mat4 *t)
{
	if (!rbuf || !t)
		return;

	/* Grow the renderbuf3D if it is too small. */
	if ((_num_vertices > rbuf->_num_vertices) ||
					(_num_indices > rbuf->_num_indices)) {
		rbuf->end();
		rbuf->flush();
		rbuf->init_vbo(_num_vertices);
		rbuf->init_ibo(_num_indices);
		rbuf->begin();

	/* Check that the current submission will not overflow the buffers. */
	} else if (((_num_vertices + rbuf->_sub_vertices) >
							rbuf->_num_vertices) ||
		   ((_num_indices + rbuf->_sub_indices) >
							rbuf->_num_indices)) {
		rbuf->end();
		rbuf->flush();
		rbuf->begin();
	}

	/* Recall the offset is used to differentiate texture samplers. */
	float id = _texture ? (float)_texture->get_offset() : -1.f;

	rbuf->insert_texture(_texture);

	for (unsigned int i = 0; i < _num_vertices; i++) {
		rbuf->_buf->pos     = *t * _vertices[i];
		rbuf->_buf->normal  = *t * _normals[i];
		rbuf->_buf->t_id    = id;
		rbuf->_buf->t_idx   = _idx;
		rbuf->_buf++->color = _color;
	}

	/* Awkward, but minimizes branching when filling tangents and uvs. */
	if (_tangents) {
		rbuf->_buf -= _num_vertices;
		if (_uv) {
			for (unsigned int i = 0; i < _num_vertices; i++) {
				rbuf->_buf->tangent = *t * _tangents[i];
				rbuf->_buf++->uv = _uv[i];
			}
		} else {
			for (unsigned int i = 0; i < _num_vertices; i++)
				rbuf->_buf++->tangent = *t * _tangents[i];
		}
	} else if (_uv) {
		rbuf->_buf -= _num_vertices;
		for (unsigned int i = 0; i < _num_vertices; i++)
			rbuf->_buf++->uv = _uv[i];
	}

	for (unsigned int i = 0; i < _num_indices; i++) {
		*(rbuf->_buf_idx) = _indices[i];
		rbuf->_buf_idx++;
	}

	rbuf->_sub_vertices += _num_vertices;
	rbuf->_sub_indices += _num_indices;
}

/*
 * compute_bases:
 * Compute vertex normals based on triangle faces and compute tangents
 * using the vertex normals and uv coordinates.
 */
void Mesh::compute_bases()
{
	if (!_vertices || !_indices)
		return;

	vector<vec3> faces;
	vec3 *face_normals;
	if (_normals)
		goto compute_tangents;

	for (unsigned int i = 0; i < _num_indices; i++)
		faces.push_back(_vertices[_indices[i]]);

	/* Get face normals. */
	face_normals = (vec3*)malloc((_num_indices / 3) * sizeof(vec3));
	if (!face_normals) {
		fprintf(stderr, "malloc failed.\n");
		return;
	}

	for (unsigned int i = 0; i < _num_indices; i += 3)
		face_normals[i / 3] = normalize(cross(faces[i + 1] - faces[i],
						faces[i + 2] - faces[i]));

	/*
	 * Compute the vertex normals by adding together the vertex's
	 * associated face normals. Then, normalize the resulting vector.
	 */
	_normals = (vec3 *)malloc(sizeof(vec3) * _num_vertices);
	for (unsigned int i = 0; i < _num_vertices; i++)
		_normals[i] = vec3(0.f, 0.f, 0.f);

	for (unsigned int i = 0; i < _num_indices; i += 3) {
		int t = i / 3;
		_normals[_indices[i]] += face_normals[t];
		_normals[_indices[i + 1]] += face_normals[t];
		_normals[_indices[i + 2]] += face_normals[t];
	}

	for (unsigned int i = 0; i < _num_vertices; i++)
		_normals[i].normalize();

	free(face_normals);

compute_tangents:

	/* Need texture coordinates for tangent. */
	if (_tangents || !_uv)
		return;

	// source: www.marti.works/calculating-tangents-for-your-mesh/
	_tangents = (vec3 *)malloc(sizeof(vec3) * _num_vertices);
	if (!_tangents) {
		fprintf(stderr, "malloc failed.\n");
		return;
	}

	for (unsigned int i = 0; i < _num_vertices; i++)
		_tangents[i] = vec3(0.f, 0.f, 0.f);

// perhaps use vec4 so that w indicates whether to invert

	vector<vec3> tanA(_num_vertices, vec3(0.f, 0.f, 0.f));
	vector<vec3> tanB(_num_vertices, vec3(0.f, 0.f, 0.f));

	for (unsigned int i = 0; i < _num_indices; i += 3) {
		vec3 pos0 = _vertices[_indices[i]];
		vec3 pos1 = _vertices[_indices[i + 1]];
		vec3 pos2 = _vertices[_indices[i + 2]];

		vec2 tex0 = _uv[_indices[i]];
		vec2 tex1 = _uv[_indices[i + 1]];
		vec2 tex2 = _uv[_indices[i + 2]];

		vec3 edge1 = pos1 - pos0;
		vec3 edge2 = pos2 - pos0;

		vec2 uv1 = tex1 - tex0;
		vec2 uv2 = tex2 - tex0;

		float r = 1.f / (uv1.x * uv2.y - uv1.y * uv2.x);

		vec3 tangent(((edge1.x * uv2.y) - (edge2.x * uv1.y)) * r,
			     ((edge1.y * uv2.y) - (edge2.y * uv1.y)) * r,
			     ((edge1.z * uv2.y) - (edge2.z * uv1.y)) * r);

		vec3 bitangent(((edge1.x * uv2.x) - (edge2.x * uv1.x)) * r,
			       ((edge1.y * uv2.x) - (edge2.y * uv1.x)) * r,
			       ((edge1.z * uv2.x) - (edge2.z * uv1.x)) * r);

		tanA[_indices[i]] += tangent;
		tanA[_indices[i + 1]] += tangent;
		tanA[_indices[i + 2]] += tangent;

		tanB[_indices[i]] += bitangent;
		tanB[_indices[i + 1]] += bitangent;
		tanB[_indices[i + 2]] += bitangent;
	}

	for (unsigned int i = 0; i < _num_vertices; i++) {
		vec3 tmp_n = _normals[i];
		vec3 t0 = tanA[i];
		vec3 t1 = tanB[i];

		vec3 tmp_t = normalize(t0 - (tmp_n * dot(tmp_n, t0)));
		_tangents[i] = tmp_t;


// for using vec4, w indicates whether to invert:
//		vec3 c = cross(n, t0);
//		float w = (dot(c, t1) < 0) ? -1.f : 1.f;
//		_tangents[i] = vec3(tmp_t.x, tmp_t.y, tmp_t.z, w);
	}
}

} /* namespace dengine */
