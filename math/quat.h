/*
-----------------------------------------------------------------------------
This source file is part of OGRE
    (Object-oriented Graphics Rendering Engine)
For the latest info, see http://www.ogre3d.org/

Copyright (c) 2000-2014 Torus Knot Software Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef _QUAT_H_
#define _QUAT_H_

#include <math/dmath.h>
#include <math/vec.h>
#include <math/mat.h>

namespace dengine {

enum AngleOrder {
	XYZ,
	YZX,
	ZYX
};

class Quat {
public:
	inline Quat() : w(1.f), x(0.f), y(0.f), z(0.f) {}
	inline Quat(float real, float ic, float jc, float kc)
		: w(real), x(ic), y(jc), z(kc) {}
	inline Quat(const mat3& rotation_matrix)
	{
		this->from_rotation_matrix(rotation_matrix);
	}
	inline Quat(float radians, const vec3& axis)
	{
		this->from_angle_axis(radians, axis);
	}
	inline Quat(float yaw, float pitch, float roll,
						AngleOrder order = ZYX)
	{
		this->from_euler(yaw, pitch, roll, order);
	}
	/* Assumes orthonormal axes. */
	inline Quat(const vec3& x_axis, const vec3& y_axis, const vec3& z_axis)
	{
		this->from_axes(x_axis, y_axis, z_axis);
	}
	inline void swap(Quat& q)
	{
		float a, b, c, d;
		a = w;
		b = x;
		c = y;
		d = z;
		w = q.w;
		x = q.x;
		y = q.y;
		z = q.z;
		q.w = a;
		q.x = b;
		q.y = c;
		q.z = d;
	}

	inline float operator[](const int idx) const { return *(&w + idx); }
	inline float& operator[](const int idx) { return *(&w + idx); }

	void from_rotation_matrix(const mat3& m);
	void get_rotation_matrix(mat3& m) const;
	void get_rotation_matrix(mat4& m) const;
	mat4 get_rotation_matrix() const;
//	void to_rotation_matrix(mat3& m) const;
//	void to_rotation_matrix(mat4& m) const;
	void from_euler(float yaw, float pitch, float roll, AngleOrder order);
	// sets up the quat using supplied vector, then "rolls" around that
	// vector by specified radians.
	void from_angle_axis(float radians, const vec3& axis);

	void to_angle_axis(float& radians, vec3& axis) const;

	// constructs quat using 3 axes, assumed to be orthonormal
	void from_axes(const vec3 *axis);
	void from_axes(const vec3& x_axis, const vec3& y_axis, const vec3& z_axis);

	// gets the 3 orthonormal axes defining the quat.
	void to_axes(vec3 *axis) const;
	void to_axes(vec3& x_axis, vec3& y_axis, vec3& z_axis) const;

	// returns the T orthonormal axis defining the quat.
	// same as doing T_axis = unit_T * this. Also called local T-axis.
	// where T = x, y, or z.
	vec3 get_x_axis() const;
	vec3 get_y_axis() const;
	vec3 get_z_axis() const;

	inline Quat& operator=(const Quat& q)
	{
		if (this == &q)
			return *this;

		w = q.w;
		x = q.x;
		y = q.y;
		z = q.z;

		return *this;
	}
	Quat operator+(const Quat& q) const;
	Quat operator-(const Quat& q) const;
	Quat operator*(const Quat& q) const;
	Quat operator*(float scalar) const;
	Quat& operator*=(float scalar)
	{
		w *= scalar;
		x *= scalar;
		y *= scalar;
		z *= scalar;
		return *this;
	}
	Quat& operator+=(const Quat& q)
	{
		w += q.w;
		x += q.x;
		y += q.y;
		z += q.z;
		return *this;
	}
	Quat& operator-=(const Quat& q)
	{
		w -= q.w;
		x -= q.x;
		y -= q.y;
		z -= q.z;
		return *this;
	}
	friend Quat operator*(float scalar, const Quat& q);
	Quat operator-() const;
	inline int operator==(const Quat& q) const
	{
		return w == q.w && x == q.x && y == q.y && z == q.z;
	}
	inline int operator!=(const Quat& q) const { return !(*this == q); }

	float dot(const Quat& q) const;
	float length() const; // returns length
	float length_squared() const; // returns normal length
//	float normalize(); // normalizes and returns previous length
	Quat& normalize();
	Quat inverse() const; // apply to non-zero quaternion
	Quat unit_inverse() const; // conjugate
	inline Quat conjugate() const { return this->unit_inverse(); }
	Quat exp() const;
	Quat log() const;
	// rotation of a vector by a quaternion
	vec3 operator*(const vec3& v) const;
	vec4 operator*(const vec4& v) const;
	/*
	 * Calculate the local roll/pitch/yaw element of this quaternion.
	 * @param reproject_axis By default the method returns the 'intuitive' result
	 * that is, if you projected the local Y of the quaternion onto the X and
	 * Y axes, the angle between them is returned. If set to false though, the
	 * result is the actual yaw that will be used to implement the quaternion,
	 * which is the shortest possible path to get to the same orientation and
	 * may involve less axial rotation.  The co-domain of the returned value is
	 * from -180 to 180 degrees.
	 */
	float get_roll(int reproject_axis = 1) const;
	float get_pitch(int reproject_axis = 1) const;
	float get_yaw(int reproject_axis = 1) const;

	// equality with tolerance (max angle difference)
//	int equals(const Quat& q, float radians) const;

	/*
	 * Performs spherical linear interpolation between two quaternions and
	 * returns the result.
	 * Example:
	 *	slerp(0.f, A, B) = A
	 *	slerp(1.f, A, B) = B
	 * Note: slerp performs interpolation at constant velocity and being
	 * torque-minimal (unless shortest_path = false). However, it is not
	 * commutative, meaning slerp(.75f, A, B) != slerp(.25f, B, A).
	 */
	Quat slerp(float ft, const Quat& q1, const Quat& q2,
							int shortest_path = 0);
	/*
	 * Adds extra "spins" (i.e. rotates several times) specified by n_extra
	 * while interpolating before arriving to final values.
	 */
	Quat slerp_extra(float ft, const Quat& q1, const Quat& q2,
								int n_extra);
	/* Setup for spherical quadratic interpolation */
//	void Intermediate(const Quat& q1, const Quat& q2, const Quat& q3,
//							const Quat& q4);
	void intermediate(const Quat& q1, const Quat& q2, const Quat& q3,
							Quat& qa, Quat& qb);

	/* Spherical quadratic interpolation */
	Quat squad(float ft, const Quat& q1, const Quat& q2, const Quat& q3,
					const Quat& q4, int shortest_path = 0);
	/*
	 * Performs normalized linear interpolation between two quaternions
	 * and returns the result.
	 * Example:
	 *	nlerp(0.f, A, B) = A
	 *	nlerp(1.f, A, B) = B
	 * Note: nlerp is faster than slerp. nlerp is commutative and torque
	 * minimal (unless shortest_path == false). However, it performs at
	 * non-constant velocity.
	 */
	Quat nlerp(float ft, const Quat& q1, const Quat& q2,
							int shortest_path = 0);

	static const float EPSILON; /* Cutoff for sine near zero. */
	static const Quat ZERO;
	static const Quat IDENTITY;

	float w, x, y, z;

	inline void print(FILE *file = stdout) const
	{
		fprintf(file, "<%.3f, %.3f, %.3f, %.3f>\n", w, x, y, z);
	}
};

} /* namespace dengine */

#endif /* _QUAT_H_ */
