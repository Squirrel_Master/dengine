#ifndef _TEXTURE_ARRAY_H_
#define _TEXTURE_ARRAY_H_

#include <vector>
#include <ft2build.h>
#include <graphics/texture.h>
#include <graphics/font.h>
#include <io/io.h>

#include FT_FREETYPE_H

using std::vector;

namespace dengine {

#define MAX_WIDTH 4096

/*
 * TextureArray:
 * TextureArray creates an array texture to minimize texture switches.
 * Currently, TextureArray will build an array texture for either regular
 * "image" textures (by way of a vector of ImageData) or for fonts (by
 * utilizing vectors of Fonts and their corresponding FT_Face data). Be
 * sure to provide the correct layout offsets for the shader.
 */
class TextureArray : public Texture {
public:
	TextureArray() {};
	TextureArray(vector<ImageData *>& images,
		     int max_w = 0,
		     int max_h = 0,
		     int layout_offset = 0);
	TextureArray(vector<Font>& fonts,
		     vector<FT_Face>& faces,
		     int layout_offset = 0);
	int init_images(vector<ImageData *>& images,
		        int max_w = 0,
			int max_h = 0,
			int layout_offset = 0);
	int init_fonts(vector<Font>& fonts,
		       vector<FT_Face>& faces,
		       int layout_offset = 0);
	virtual void bind();
	virtual void unbind();
};

} /* namespace dengine */

#endif /* _TEXTURE_ARRAY_H_ */
