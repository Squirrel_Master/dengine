#ifndef _TEXTURE2D_H_
#define _TEXTURE2D_H_

#include <io/io.h>
#include <graphics/texture.h>

namespace dengine {

class Texture2D : public Texture {
public:
	Texture2D() {};
	Texture2D(ImageData &image, int layout_offset = 0);
	int init(ImageData &image, int layout_offset = 0);
	virtual void bind();
	virtual void unbind();
};

} /* namespace dengine */

#endif /* _TEXTURE2D_H_ */
