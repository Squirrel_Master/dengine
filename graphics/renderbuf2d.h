#ifndef _RENDERBUF2D_H_
#define _RENDERBUF2D_H_

#include <math/dmath.h>
#include <math/vec.h>
#include <graphics/vertex.h>
#include <vector>
#include <unordered_set>
#include <graphics/texture.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

namespace dengine {

#define MAX_RENDERABLES	60000
#define VERTEX_SIZE sizeof(Vertex3)
#define RENDERABLE_SIZE	(VERTEX_SIZE * 4)
#define BUFFER_SIZE	(RENDERABLE_SIZE * MAX_RENDERABLES)
#define INDICES_SIZE	(MAX_RENDERABLES * 6)

/* Position of color and pos in struct vertex */
#define POSITION_OFFSET BUFFER_OFFSET(Vertex3, pos)
#define UV_OFFSET       BUFFER_OFFSET(Vertex3, uv)
#define TID_OFFSET      BUFFER_OFFSET(Vertex3, t_id)
#define TIDX_OFFSET     BUFFER_OFFSET(Vertex3, t_idx)
#define COLOR_OFFSET    BUFFER_OFFSET(Vertex3, color)

/* Layout position in the shader. */
#define POSITION_INDEX 0
#define UV_INDEX       3
#define TID_INDEX      4
#define TIDX_INDEX     5
#define COLOR_INDEX    6

using std::vector;
using std::unordered_set;
typedef unordered_set<Texture *>::iterator set_iter;

/*
 * Renderbuf2D:
 * Encapsulates the OpenGL portion of the renderer (which now deals primarily
 * with the batch hierarchy of renderables). Additionally, now submit methods
 * belong to the renderables rather than the renderer, allowing for easy
 * use of inheritance (as opposed to using typeof, typeid, setjmp/longjmp, and
 * so forth). For instance, submitting a standard Sprite is easy, but for
 * a Font, which could be a subclass of Sprite, additional submissions per
 * character in the string (each glyph) must be accounted for.
 */
class Renderbuf2D {
public:
	Renderbuf2D();
	~Renderbuf2D();

	/* init(): initializes the Renderbuf2D. */
	void init(const vector<GLuint> *all_indices = NULL);

	/* destroy(): deletes all allocated variables. */
	void destroy();

	/* Call at the very beginning of any rendering routine. */
	void reset() { _draw_calls = 0; }

	void begin();
	void end();
	void flush();
	void insert_texture(Texture *t);

	Vertex3 *_buf;

	GLuint _vao, _vbo, _ibo;

	/* Maintain set of texture slots in shader (constant lookup). */
	unordered_set<Texture *> _tslots;

	int _draw_calls;
	int _num_vertices, _num_indices;

	int _sub_count;			/* Count of submitted renderables. */
	unsigned int _sub_indices;	/* Count of submitted indices. */
};

}

#endif /* _RENDERBUF2D_H_ */

