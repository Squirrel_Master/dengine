#include <graphics/texture3d.h>

namespace dengine {

Texture3D::Texture3D(vector<ImageData *>& images, int max_w, int max_h,
							int layout_offset)
{
	init(images, max_w, max_h);
}

int Texture3D::init(vector<ImageData *>& images, int max_w, int max_h,
							int layout_offset)
{
	if (layout_offset < 0) {
		int max;
		glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &max);
		fprintf(stderr, "Texture layout must be between 0 and %d\n",
									max);
		return -1;
	}

	if (images.size() < 1) {
		fprintf(stderr, "No images to send to Texture3D\n");
		return -1;
	}

	int bpp = images[0]->get_bpp();

	/* Iterate through the images and find the maximum dimensions. */
	if (max_w < 1 || max_h < 1) {
		for (int i = 0; i < images.size(); i++) {
			int w = images[i]->get_width();
			int h = images[i]->get_height();
			if (w < 1 || h < 1 || images[i]->get_bpp() != bpp)
				return -1;
			if (w > max_w)
				max_w = w;
			if (h > max_h)
				max_h = h;
		}
	}

	_width = max_w;
	_height = max_h;
	_depth = images.size();

	_offset = layout_offset;
	glActiveTexture(GL_TEXTURE0 + _offset);
	glBindTexture(GL_TEXTURE_3D, _id);

	GLenum in_format, out_format, out_type;

	/*
	 * Set the internalFormat (in_format), format of pixel data
	 * (out_format), and the data type of the pixel data based on bits per
	 * pixel. These settings seem to be the most common.
	 */
	switch (bpp) {
	case 8:
		in_format = GL_R3_G3_B2;
		out_format = GL_RGB;
		out_type = GL_UNSIGNED_BYTE_3_3_2;
		break;
	case 16:
		in_format = GL_RGB5;
		out_format = GL_RGB;
		out_type = GL_UNSIGNED_SHORT_5_6_5;
		break;
	case 24:
		in_format = GL_RGB;
		out_format = GL_BGR;
		out_type = GL_UNSIGNED_BYTE;
		break;
	default:
		in_format = GL_RGBA;
		out_format = GL_BGRA;
		out_type = GL_UNSIGNED_BYTE;
		break;
	}

	/* Create the 3D texture. */
	glTexImage3D(GL_TEXTURE_3D,	/* GLenum target */
		     0,			/* GLint level */
		     in_format,		/* GLint internalFormat */
		     _width,		/* GLsizei width */
		     _height,		/* GLsizei height */
		     _depth,		/* GLsizei depth */
		     0,			/* GLint border */
		     out_format,	/* GLenum format */
		     out_type,		/* GLenum type */
		     NULL);		/* const GLvoid *data */

	/*
	 * NOTE: perhaps send the vector images instead of NULL
	 *       to glTexImage3D rather than utilizing glTexSubImage3D.
	 *       Would, of course, create a cube, thus maybe look into
	 *       cubemaps.
	 */
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	/* Send the image data to the 3D texture. */
	for (int i = 0; i < images.size(); i++) {
		glTexSubImage3D(GL_TEXTURE_3D,
				0,
				0,
				0,
				i,
				images[i]->get_width(),
				images[i]->get_height(),
				1,
				out_format,
				out_type,
				images[i]->get_pixels());
	}

	glBindTexture(GL_TEXTURE_3D, 0);

	return 0;
}

void Texture3D::bind()
{
	if (!_bound) {
		glActiveTexture(GL_TEXTURE0 + _offset);
		glBindTexture(GL_TEXTURE_3D, _id);
		_bound = 1;
	}
}

void Texture3D::unbind()
{
	if (_bound) {
		glActiveTexture(GL_TEXTURE0 + _offset);
		glBindTexture(GL_TEXTURE_3D, 0);
		_bound = 0;
	}
}

} /* namespace dengine */
