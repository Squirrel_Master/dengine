#ifndef _FONT_MANAGER_H_
#define _FONT_MANAGER_H_

#include <stdlib.h>
#include <stdio.h>
#include <GL/glew.h>
#include <vector>
#include <ft2build.h>
#include <graphics/texture_array.h>
#include <graphics/font.h>

#include FT_FREETYPE_H
using std::vector;

namespace dengine {

/*
 * FontManager:
 * Encapsulates all use of the FreeType2 API. Rather than keeping FreeType2
 * active and loading each character and so forth dynamically through
 * FreeType2, a particular font's character set is loaded at a particular
 * size. The aim is to collect all desired fonts, create texture atlases
 * for their character sets, and build an array texture from all of them,
 * at which point FreeType2's resources are released.  This, of course,
 * means that text clarity is based almost entirely on the initial pixel
 * height.
 */
class FontManager {
public:
	FontManager();
	~FontManager();
	void destroy();

	int add_font(const char *fontname,
		     const char *filename,
		     int pixel_height);
	Font *get_font(const char *fontname);

	int build_texture_array(int layout_offset = 0);

private:
	int init_freetype();

	FT_Library _ftlib;
	vector<Font> _fonts;
	vector<FT_Face> _faces;
	vector<TextureArray *> _textures;
};

} /* namespace dengine */

#endif /* _FONT_MANAGER_H_ */
