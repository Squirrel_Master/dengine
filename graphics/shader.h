#ifndef _SHADER_H
#define _SHADER_H

#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GL/freeglut.h>

#include <io/io.h>
#include <math/dmath.h>
#include <math/vec.h>
#include <math/mat.h>
#include <string.h>
#include <unordered_map>

namespace dengine {

using std::unordered_map;
typedef unordered_map<unsigned long, GLint>::iterator map_iter;

class Shader {
public:
        Shader() : _pid(0), _vid(0), _fid(0), _attr(0), _enabled(0) {};
        ~Shader();

        int init(const char *file_v, const char *file_f);
        int link();
        void add_attribute(const char *name);
        void enable();
        void disable();
	void destroy();

	/* 1-dimensional uniforms */
	GLint set_uniform_1f(const char *name, const float val);
	GLint set_uniform_1i(const char *name, const int val);
	void set_uniform_1f(GLint loc, const float val);
	void set_uniform_1i(GLint loc, const int val);

	/* Vector uniforms */
	GLint set_uniform_2f(const char *name, const vec2 &v);
	GLint set_uniform_3f(const char *name, const vec3 &v);
	GLint set_uniform_4f(const char *name, const vec4 &v);
	GLint set_uniform_1iv(const char *name, const int count, GLuint *v);
	void set_uniform_2f(GLint loc, const vec2 &v);
	void set_uniform_3f(GLint loc, const vec3 &v);
	void set_uniform_4f(GLint loc, const vec4 &v);
	void set_uniform_1iv(GLint loc, const int count, GLuint *v);

	/* Matrix uniforms */
	GLint set_uniform_mat4(const char *name, const mat4 &m,
					GLboolean transpose = GL_FALSE);
	void set_uniform_mat4(GLint loc, const mat4 &m,
					GLboolean transpose = GL_FALSE);

private:
        GLint get_uniform_loc(const char *uniform_name);
        int compile(const char *filename, GLuint id);

        /* Program and shader IDs */
        GLuint _pid, _vid, _fid;

        /* Number of attributes */
        int _attr;

	/* Enable flag */
	int _enabled;

	/*
	 * Map of uniform offset locations to avoid the dreaded
	 * glGetUniformLocation() calls.
	 */
	unordered_map<unsigned long, GLint> _uniform_map;
};

} /* namespace dengine */

#endif /* _SHADER_H */
