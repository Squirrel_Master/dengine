#ifndef _RENDERER_H_
#define _RENDERER_H_

#include <vector>

#include <math/dmath.h>
#include <math/mat.h>
#include <graphics/shader.h>
#include <graphics/batch.h>
#include <memory>

using std::shared_ptr;

namespace dengine {

/*
 * class Renderer:
 * Renderer is an abstract class that facilitates the sending of renderables
 * to the appropriate shader.  It is intended to dictate the organization
 * of other renderers (most, if not all, of which should implement it).
 */
template <class T>
class Renderer {
public:
        Renderer() : _shader(NULL) {};
        virtual ~Renderer() {};
	virtual void add(Batch<T> *parent, Batch<T> *child) = 0;
	virtual void remove(Batch<T> *batch) = 0;
	virtual void add_shader(Shader *s)
	{
		if (s)
			_shader = s;
	}
        virtual void render() = 0;
        virtual void init() = 0;
        virtual void destroy() = 0;

	inline int contains(const Batch<T> *batch) const
	{
		return _root.find(batch) != NULL;
	}
	inline int contains(const T *renderable) const
	{
		return _root.find(renderable) != NULL;
	}

protected:
	virtual void submit(Batch<T> *batch) = 0;
	Batch<T> _root;

	Shader *_shader;
};

} /* namespace dengine */

#endif /* _RENDERER_H_ */
