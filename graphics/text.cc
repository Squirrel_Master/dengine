#include <graphics/text.h>

namespace dengine {

Text::Text() : _font(NULL),
	       _str(NULL),

	      /* Default scale is 1 in 16:9 aspect ratio. */
	       _aspect(16.f * 2.f / glutGet(GLUT_WINDOW_WIDTH),
		       9.f * 2.f / glutGet(GLUT_WINDOW_HEIGHT)),
	       _scale(_aspect)
{
}

Text::Text(const char *str,
	   const Font *font,
	   const vec3& position,
	   const vec2& scale,
	   const vec2& aspect,
	   const vec4& color) : _font(NULL), _str(NULL)
{
	if (set_text(str, font, position, scale, aspect, color) < 0)
		fprintf(stderr, "Text::set_text failed.\n");
}

Text::~Text()
{
	destroy();
}

/*
 * destroy:
 * Frees all dynamically allocated memory.
 */
void Text::destroy()
{
	if (_str) {
		free(_str);
		_str = NULL;
	}
}

/*
 * set_text:
 * Specifies the ASCII string, desired font, position, dimensions, and color
 * of the text to render.
 */
int Text::set_text(const char *str,
		   const Font *font,
		   const vec3& position,
		   const vec2& scale,
		   const vec2& aspect,
		   const vec4& color)
{
	if (!str || !font)
		return -1;

	/*
	 * The aspect ratio is important for properly scaling text.  As this is
	 * not intended to be a word processing application, only the initial
	 * aspect ratio is required, which will lead to stretching of the
	 * text on window resizing (as opposed to the goal of maintaining the
	 * same size regardless in word processing).
	 */
	if (!(aspect.x > 0.f && aspect.y > 0.f)) {
		fprintf(stderr, "Aspect coordinates must be greater than 0.\n");
		return -1;
	}

	if (color.x < 0.f || color.y < 0.f || color.z < 0.f || color.w < 0.f) {
		fprintf(stderr, "Color coordinates must be between 0 and 1.\n");
		return -1;
	}

	/* Pack the 4 4-byte floats into a single 4-byte int. */
	set_color(color.x, color.y, color.z, color.w);

	_pos = position;
	_font = font;

	set_aspect(aspect.x, aspect.y);
	set_scale(scale.x, scale.y);

	/*
	 * Set _str to the desired text, contained in str.  A buffer would of
	 * course be faster, but hopefully loads of text will not be changing
	 * constantly (which would require many malloc and free calls).
	 */
	if (_str)
		free(_str);

	int len = strlen(str);
	if (!(_str = (char *)malloc(len + 1))) {
		fprintf(stderr, "malloc failed.\n");
		return -1;
	}
	_str[len] = '\0';
	strcpy(_str, str);

	return 0;
}

/*
 * set_text:
 * Set _str to the desired text, contained in str.  A buffer would of
 * course be faster, but hopefully loads of text will not be changing
 * constantly (which would require many malloc and free calls).
 */
int Text::set_text(const char *str, const Font *font)
{
	if (!str)
		return -1;

	/* Ensure that either a new font is being set or already have a font. */
	if (!font) {
		if (!_font)
			return -1;
	} else {
		_font = font;
	}

	if (_str)
		free(_str);

	int len = strlen(str);
	if (!(_str = (char *)malloc(len + 1))) {
		fprintf(stderr, "malloc failed.\n");
		return -1;
	}
	_str[len] = '\0';
	strcpy(_str, str);
}

/*
 * append_text:
 * Appends the input string to the current string.
 */
int Text::append_text(const char *str)
{
	if (!str)
		return -1;

	char *new_str;
	int len = strlen(str) + 1;

	if (_str)
		len += strlen(_str);

	new_str = (char *)malloc(len);
	if (!new_str) {
		fprintf(stderr, "malloc failed.\n");
		return -1;
	}

	new_str[0] = '\0';

	if (_str)
		strcat(new_str, _str);

	strcat(new_str, str);
	free(_str);
	_str = new_str;

	return 0;
}

/*
 * set_aspect:
 * Updates the current aspect ratio, accounting for the current
 * window dimensions.
 */
void Text::set_aspect(float x, float y)
{
	_aspect.x = fabs(x) * 2.f / glutGet(GLUT_WINDOW_WIDTH);
	_aspect.y = fabs(y) * 2.f / glutGet(GLUT_WINDOW_HEIGHT);
}

/*
 * scale:
 * Updates the current scale values for scaling (resizing) text.
 */
void Text::set_scale(float x, float y)
{
	_scale.x = fabs(x) * _aspect.x;
	_scale.y = fabs(y) * _aspect.y;
}

/*
 * submit:
 * Responsible for submitting the appropriate texture data and coordinates to
 * the Renderbuf2D.
 */
void Text::submit(Renderbuf2D *rbuf, mat4 *t)
{
	if (!rbuf || !t)
		return;

	/* Each glyph is a submission, so count them all. */
	if ((rbuf->_sub_count += strlen(_str)) > MAX_RENDERABLES) {
		rbuf->end();
		rbuf->flush();
		rbuf->begin();
		rbuf->_sub_count = strlen(_str);
	}

	rbuf->insert_texture(_font->_texture);
	float x = _pos.x;
	float id = (float)_font->_texture->get_offset();

	for (char *p = _str; *p; p++) {

		float x0 = x + _font->c[*p].bl * _scale.x;
		float y0 = _pos.y + _font->c[*p].bt * _scale.y;
		float x1 = x0 + _font->c[*p].bw * _scale.x;
		float y1 = y0 - _font->c[*p].bh * _scale.y;

		float u0 = _font->c[*p].tx;
		float v0 = _font->c[*p].ty;
		float u1 = u0 + _font->c[*p].bw / _font->_texture->get_width();
		float v1 = v0 + _font->c[*p].bh / _font->_texture->get_height();

		/* Vertex 1 */
		rbuf->_buf->pos = *t * vec3(x0, y0, _pos.z);
		rbuf->_buf->uv = vec2(u0, v0);
		rbuf->_buf->t_id = id;
		rbuf->_buf->t_idx = (float)_font->_idx;
		rbuf->_buf++->color = _color;

		/* Vertex 2 */
		rbuf->_buf->pos = *t * vec3(x0, y1, _pos.z);
		rbuf->_buf->uv = vec2(u0, v1);
		rbuf->_buf->t_id = id;
		rbuf->_buf->t_idx = (float)_font->_idx;
		rbuf->_buf++->color = _color;

		/* Vertex 3 */
		rbuf->_buf->pos = *t * vec3(x1, y1, _pos.z);
		rbuf->_buf->uv = vec2(u1, v1);
		rbuf->_buf->t_id = id;
		rbuf->_buf->t_idx = (float)_font->_idx;
		rbuf->_buf++->color = _color;

		/* Vertex 4 */
		rbuf->_buf->pos = *t * vec3(x1, y0, _pos.z);
		rbuf->_buf->uv = vec2(u1, v0);
		rbuf->_buf->t_id = id;
		rbuf->_buf->t_idx = (float)_font->_idx;
		rbuf->_buf++->color = _color;

		rbuf->_sub_indices += 6;

		x += _font->c[*p].ax * _scale.x;
	}
}

void Text::submit(Renderbuf3D *rbuf, mat4 *t)
{
	if (!rbuf || !t)
		return;

	unsigned int len = strlen(_str);
	unsigned int num_vertices = 4 * len;
	unsigned int num_indices = 6 * len;

	/* Grow the renderbuf3D if it is too small. */
	if ((rbuf->_num_vertices < num_vertices) || (rbuf->_num_indices < num_indices)) {
		printf("Text growing!!!\n");
		rbuf->end();
		rbuf->flush();
		rbuf->init_vbo(num_vertices);
		rbuf->init_ibo(num_indices);
		rbuf->begin();

	/* Check that the current submission will not overflow the buffers. */
	} else if (((rbuf->_sub_vertices + num_vertices) > rbuf->_num_vertices) ||
		   ((rbuf->_sub_indices + num_indices) > rbuf->_num_indices)) {
		rbuf->end();
		rbuf->flush();
		rbuf->begin();
	}

	rbuf->insert_texture(_font->_texture);
	float x = _pos.x;
	float id = (float)_font->_texture->get_offset();

	unsigned int v_offset = rbuf->_sub_vertices;

	for (char *p = _str; *p; p++, v_offset += 4) {

		float x0 = x + _font->c[*p].bl * _scale.x;
		float y0 = _pos.y + _font->c[*p].bt * _scale.y;
		float x1 = x0 + _font->c[*p].bw * _scale.x;
		float y1 = y0 - _font->c[*p].bh * _scale.y;

		float u0 = _font->c[*p].tx;
		float v0 = _font->c[*p].ty;
		float u1 = u0 + _font->c[*p].bw / _font->_texture->get_width();
		float v1 = v0 + _font->c[*p].bh / _font->_texture->get_height();

		/* Vertex 1 */
		rbuf->_buf->pos = *t * vec3(x0, y0, _pos.z);
		rbuf->_buf->uv = vec2(u0, v0);
		rbuf->_buf->t_id = id;
		rbuf->_buf->t_idx = (float)_font->_idx;
		rbuf->_buf++->color = _color;

		/* Vertex 2 */
		rbuf->_buf->pos = *t * vec3(x0, y1, _pos.z);
		rbuf->_buf->uv = vec2(u0, v1);
		rbuf->_buf->t_id = id;
		rbuf->_buf->t_idx = (float)_font->_idx;
		rbuf->_buf++->color = _color;

		/* Vertex 3 */
		rbuf->_buf->pos = *t * vec3(x1, y1, _pos.z);
		rbuf->_buf->uv = vec2(u1, v1);
		rbuf->_buf->t_id = id;
		rbuf->_buf->t_idx = (float)_font->_idx;
		rbuf->_buf++->color = _color;

		/* Vertex 4 */
		rbuf->_buf->pos = *t * vec3(x1, y0, _pos.z);
		rbuf->_buf->uv = vec2(u1, v0);
		rbuf->_buf->t_id = id;
		rbuf->_buf->t_idx = (float)_font->_idx;
		rbuf->_buf++->color = _color;

		x += _font->c[*p].ax * _scale.x;

		rbuf->_buf_idx[0] = v_offset;
		rbuf->_buf_idx[1] = v_offset + 1;
		rbuf->_buf_idx[2] = v_offset + 2;
		rbuf->_buf_idx[3] = v_offset + 2;
		rbuf->_buf_idx[4] = v_offset + 3;
		rbuf->_buf_idx[5] = v_offset;

		rbuf->_buf_idx += 6;
	}

	rbuf->_sub_vertices += num_vertices;
	rbuf->_sub_indices += num_indices;
}

} /* namespace dengine */
