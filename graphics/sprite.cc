#include <graphics/sprite.h>

namespace dengine {

Sprite::Sprite(const vec3& position,
	       const vec2& size,
	       const vec4& color,
	       Texture *t,
	       const int index,
	       const ImageData *image)
{
	init(position, size, color, t, index, image);
}

Sprite::~Sprite()
{
	destroy();
}

void Sprite::destroy()
{
	_width = _height = 0.f;
	for (int i = 0; i < 4; i++)
		_uv[i] = vec2(0.f, 0.f);
	_texture = NULL;
	_idx = -1.f;
}

int Sprite::init(const vec3& position,
		  const vec2& size,
		  const vec4& color,
		  Texture *t,
		  const int index,
		  const ImageData *image)
{
        _pos = position;

	set_color(color.x, color.y, color.z, color.w);
	set_width(size.x);
	set_height(size.y);

	/*
	 * Set the default texture coordinates (uv). Bear in mind,
	 * for proper scaling within the texture, image should point
	 * to the correct image as indicated by t and the index. For
	 * instance, if t points to a Texture2D_Array and image refers
	 * to the texture at level 1, then index should be 1, otherwise
	 * the texture will be stretched or cropped based on the dimensions
	 * of the Texture2D_Array (recall that the height and width of
	 * a Texture2D_Array is the maximum height and width of all its
	 * 'images').
	 */
	float u = 1.f;
	float v = 1.f;
	if (t && image) {
		u = image->get_width() / t->get_width();
		v = image->get_height() / t->get_height();
	}
	_uv[0] = vec2(0, 0);
	_uv[1] = vec2(u, 0);
	_uv[2] = vec2(u, v);
	_uv[3] = vec2(0, v);

	_texture = t;
	_idx = (float)index;
}

void Sprite::submit(Renderbuf2D *rbuf, mat4 *t)
{
	if (!rbuf || !t)
		return;


	if (++(rbuf->_sub_count) > MAX_RENDERABLES) {
		rbuf->end();
		rbuf->flush();
		rbuf->begin();
		rbuf->_sub_count = 1;
	}

	/*
	 * Hacky way of differentiating, in the shader, between array
	 * textures (although most other solutions I've found online
	 * have been considerably more heinous).
	 */
	float id = _texture ? (float)_texture->get_offset() : -1.f;

	rbuf->insert_texture(_texture);

	/* Vertex 1 */
	rbuf->_buf->pos = *t * _pos;
	rbuf->_buf->uv = _uv[0];
	rbuf->_buf->t_id = id;
	rbuf->_buf->t_idx = _idx;
	rbuf->_buf++->color = _color;

	/* Vertex 2 */
	rbuf->_buf->pos = *t * vec3(_pos.x + _width, _pos.y, _pos.z);
	rbuf->_buf->uv = _uv[1];
	rbuf->_buf->t_id = id;
	rbuf->_buf->t_idx = _idx;
	rbuf->_buf++->color = _color;

	/* Vertex 3 */
	rbuf->_buf->pos = *t * vec3(_pos.x + _width, _pos.y + _height, _pos.z);
	rbuf->_buf->uv = _uv[2];
	rbuf->_buf->t_id = id;
	rbuf->_buf->t_idx = _idx;
	rbuf->_buf++->color = _color;

	/* Vertex 4 */
	rbuf->_buf->pos = *t * vec3(_pos.x, _pos.y + _height, _pos.z);
	rbuf->_buf->uv = _uv[3];
	rbuf->_buf->t_id = id;
	rbuf->_buf->t_idx = _idx;
	rbuf->_buf++->color = _color;

	rbuf->_sub_indices += 6;
}

void Sprite::submit(Renderbuf3D *rbuf, mat4 *t)
{
	if (!rbuf || !t)
		return;

	/* Grow the renderbuf3D if it is too small. */
	if ((rbuf->_num_vertices < 4) || (rbuf->_num_indices < 6)) {
		printf("sprite GROWING!!!\n");
		rbuf->end();
		rbuf->flush();
		rbuf->init_vbo(4);
		rbuf->init_ibo(6);
		rbuf->begin();

	/* Check that the current submission will not overflow the buffers. */
	} else if (((rbuf->_sub_vertices + 4) > rbuf->_num_vertices) ||
		   ((rbuf->_sub_indices + 6) > rbuf->_num_indices)) {
		rbuf->end();
		rbuf->flush();
		rbuf->begin();
	}

	/*
	 * Hacky way of differentiating, in the shader, between array
	 * textures (although most other solutions I've found online
	 * have been considerably more heinous).
	 */
	float id = _texture ? (float)_texture->get_offset() : -1.f;

	rbuf->insert_texture(_texture);

	/* Vertex 1 */
	rbuf->_buf->pos = *t * _pos;
	rbuf->_buf->uv = _uv[0];
	rbuf->_buf->t_id = id;
	rbuf->_buf->t_idx = _idx;
	rbuf->_buf++->color = _color;

	/* Vertex 2 */
	rbuf->_buf->pos = *t * vec3(_pos.x + _width, _pos.y, _pos.z);
	rbuf->_buf->uv = _uv[1];
	rbuf->_buf->t_id = id;
	rbuf->_buf->t_idx = _idx;
	rbuf->_buf++->color = _color;

	/* Vertex 3 */
	rbuf->_buf->pos = *t * vec3(_pos.x + _width, _pos.y + _height, _pos.z);
	rbuf->_buf->uv = _uv[2];
	rbuf->_buf->t_id = id;
	rbuf->_buf->t_idx = _idx;
	rbuf->_buf++->color = _color;

	/* Vertex 4 */
	rbuf->_buf->pos = *t * vec3(_pos.x, _pos.y + _height, _pos.z);
	rbuf->_buf->uv = _uv[3];
	rbuf->_buf->t_id = id;
	rbuf->_buf->t_idx = _idx;
	rbuf->_buf++->color = _color;

	rbuf->_buf_idx[0] = rbuf->_sub_vertices;
	rbuf->_buf_idx[1] = rbuf->_sub_vertices + 1;
	rbuf->_buf_idx[2] = rbuf->_sub_vertices + 2;
	rbuf->_buf_idx[3] = rbuf->_sub_vertices + 2;
	rbuf->_buf_idx[4] = rbuf->_sub_vertices + 3;
	rbuf->_buf_idx[5] = rbuf->_sub_vertices;

	rbuf->_buf_idx += 6;

	rbuf->_sub_vertices += 4;
	rbuf->_sub_indices += 6;
}

} /* namespace dengine */
