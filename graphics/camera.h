#ifndef _CAMERA_H_
#define _CAMERA_H_

#include <math/dmath.h>
#include <math/vec.h>
#include <math/mat.h>
#include <math/quat.h>
#include <graphics/shader.h>

namespace dengine {

enum ProjectionType {
	ORTHOGRAPHIC,
	PERSPECTIVE
};

enum MoveDirection {
	UP,
	DOWN,
	LEFT,
	RIGHT,
	FRONT,
	BACK
};

enum CameraType {
	FREE,
	FPS
};

/*
 * Camera:
 * Encapsulates the projection and view matrices and their transformations.
 * Currently, two types of cameras are offered: FREE and FPS.  FREE places
 * no restrictions on rotations and strongly resembles the movement of
 * an airplane (specifically, rotating up and left rolls the camera slightly).
 * FPS (from "first person shooter") cancels out this rolling about Z.
 *
 * Notes on the mechanisms of actions:
 *  Rotations and translations are separated since dual quaternions have
 *  not been implemented yet.  To reduce operations, up-to-date matrices
 *  are kept for rotations (_rotation), translations (_translation),
 *  view (_view), projection (_proj), and the "unified" view and projection
 *  matrix (_vp_matrix). When the camera is updated (Camera::update()),
 *  if a rotation and/or translation has occurred (checked via the flags
 *  _rotated and _moved), then _rotation and/or _translation are updated
 *  accordingly and _view is then updated.  Then, if the view or projection
 *  matrices have changed (determined via the flags _proj_changed and
 *  _view_changed), the _vp_matrix is updated and sent to the shader.
 */
class Camera {
public:
	Camera();

	/* Set the projection to be perspective or orthographic. */
	void set_projection(float aspect_width,
			    float aspect_height,
			    float znear,
			    float zfar,
			    float fov = 45.f,
			    ProjectionType pt = PERSPECTIVE);

	/* Set the shader with the appropriate VP matrix uniform name. */
	void set_shader(Shader *s, const char *uname = "vp_matrix");

	/* Translates the camera's position in a particular direction. */
	void move(MoveDirection dir);
	void move(const vec3& unit_dir);

	/* Rotates the camera given the input angles, in radians. */
	void rotate(float rad_pitch,
		    float rad_yaw,
		    float rad_roll = 0.f,
		    AngleOrder order = ZYX);

	/* Zooms the camera by altering the projection's field of view. */
	void zoom(float zoom_factor);

	/* Reset the zoom to its default setting. */
	void reset_zoom();

	/* Controls the camera's rate of movement. */
	inline void set_speed(float speed_factor)
	{
		_speed = fabs(speed_factor);
	}

	/* Controls the camera's rate of rotation. */
	inline void set_sensitivity(float sensitivity_factor)
	{
		_sensitivity = fabs(sensitivity_factor);
	}

	/* Sets the camera's orientation and sets the rotation flag. */
	void set_orientation(const Quat& q);

	/* Sets the camera's position and sets the movement flag. */
	void set_position(const vec3& v);

	/* Set the camera to either FREE or FPS. */
	inline void set_cam_type(CameraType ct) { _cam_type = ct; }

	/* Applies all changes to the camera's state. */
	void update();

	inline const Quat& get_orientation() const { return _orientation; }
	inline const vec3& get_pos() const { return _pos; }

protected:

	/* Updates the projection and sets the projection flag. */
	void update_projection();

	/* Updates the view and sets the view flag. */
	void update_view();

	mat4 _view, _proj, _vp_matrix, _rotation, _translation;

	/* Camera orientation. */
	Quat _orientation;

	/* Camera position. */
	vec3 _pos;

	/* Camera rotation sensitivity and movement speed. */
	float _speed, _sensitivity;

	/* Projection parameters. */
	float _aw, _ah, _znear, _zfar, _fov;

	/* Zoom adjustment parameters. */
	float _aw_scaled, _ah_scaled, _fov_scaled;

	Shader *_shader;
	ProjectionType _ptype;

	/* Matrix state flags. */
	int _moved, _rotated, _proj_changed, _view_changed;

	/* VP Matrix uniform location (removes the need to save the name). */
	GLint _vpid;

	/* Flag to differentiate camera operations. */
	CameraType _cam_type;
};

} /* namespace dengine */

#endif /* _CAMERA_H_ */
