#include <math/dual_quat.h>

namespace dengine {

DualQuat::DualQuat() : _real(1.f, 0.f, 0.f, 0.f), _dual(0.f, 0.f, 0.f, 0.f)
{
}

DualQuat::DualQuat(const Quat& real, const Quat& dual)
{
	_real = real;
	_real.normalize();
	_dual = dual;
}
DualQuat::DualQuat(const Quat& rot, const vec3& trans)
{
	_real = rot;
	_real.normalize();
	_dual = (Quat(0, trans) * _real) * .5f;
}
float DualQuat::dot(const DualQuat& d)
{
	return _real.dot(d._real);
}
DualQuat DualQuat::operator*(float scalar)
{
	DualQuat d = *this;
	d._real *= scalar;
	d._dual *= scalar;

	return d;
}
DualQuat& DualQuat::operator=(const DualQuat& q)
{
	if (this == &q)
		return *this;

	_real = q._real;
	_dual = q._dual;

	return *this;
}
DualQuat& DualQuat::normalize()
{
//	float mag = _real.dot(_real);
//	if (mag < 0.000001f)
//		return DualQuat(ZERO, ZERO);
	float len = _real.length_squared();
	if (len > 0.f) {
		len = 1.f / len;
		_real *= len;
		_dual *= len;
	}

	return *this;
}
DualQuat DualQuat::operator+(const DualQuat& rhs)
{
	return DualQuat(_real + rhs._real, _dual + rhs._dual);
}
DualQuat DualQuat::operator*(const DualQuat& rhs)
{
	return DualQuat(rhs._real * _real,
			rhs._dual * _real + rhs._real * _dual);
}
DualQuat DualQuat::conjugate() const
{
	return DualQuat(_real.conjugate(), _dual.conjugate());
}
Quat DualQuat::get_rotation() const
{
	return _real;
}
vec3 DualQuat::get_translation() const
{
//	Quat t(_dual * 2.f * _real.conjugate());
	Quat t = _dual * _real.conjugate() * 2.f;
	return vec3(t.x, t.y, t.z);
}
mat4 DualQuat::get_matrix()
{
	mat4 m;
	get_matrix(m);
	return m;
}
void DualQuat::get_matrix(mat4& m)
{
//	normalize();
//	_real.normalize();
	float w = _real.w;
	float x = _real.x;
	float y = _real.y;
	float z = _real.z;

	float ww = w * w;
	float xx = x * x;
	float yy = y * y;
	float zz = z * z;
	float xy = x * y;
	float wz = w * z;
	float xz = x * z;
	float wy = w * y;
	float yz = y * z;
	float wx = w * x;

/*
	// extract rotational info
	m[0][0] = ww + xx - (yy + zz);
	m[0][1] = 2 * (xy + wz);
	m[0][2] = 2 * (xz - wy);
	m[0][3] = 0.f;

	m[1][0] = 2 * (xy - wz);
	m[1][1] = ww + yy - (xx + zz);
	m[1][2] = 2 * (yz + wx);
	m[1][3] = 0.f;

	m[2][0] = 2 * (xz + wy);
	m[2][1] = 2 * (yz - wx);
	m[2][2] = ww + zz - xx - yy;
	m[2][3] = 0.f;

	// extract translational info
//	Quat t((_dual * 2.f) * _real.conjugate());
	Quat t(_real.conjugate() * (_dual * 2.f));
	m[3][0] = t.x;
	m[3][1] = t.y;
	m[3][2] = t.z;
	m[3][3] = 1.f;
*/
	// extract rotational info
	m[0][0] = ww + xx - (yy + zz);
	m[1][0] = 2 * (xy + wz);
	m[2][0] = 2 * (xz - wy);
	m[3][0] = 0.f;

	m[0][1] = 2 * (xy - wz);
	m[1][1] = ww + yy - (xx + zz);
	m[2][1] = 2 * (yz + wx);
	m[3][1] = 0.f;

	m[0][2] = 2 * (xz + wy);
	m[1][2] = 2 * (yz - wx);
	m[2][2] = ww + zz - xx - yy;
	m[3][2] = 0.f;

	// extract translational info
//	Quat t((_dual * 2.f) * _real.conjugate());
	Quat t(_real.conjugate() * (_dual * 2.f));
	m[0][3] = t.x;
	m[1][3] = t.y;
	m[2][3] = t.z;
	m[3][3] = 1.f;
}

} /* namespace dengine */
