#include <graphics/texture2d.h>

namespace dengine {

Texture2D::Texture2D(ImageData &image, int layout_offset)
{
	init(image, layout_offset);
}

int Texture2D::init(ImageData &image, int layout_offset)
{
	if (layout_offset < 0 || image.get_width() < 1 ||
						image.get_height() < 1) {
		int max;
		glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &max);
		fprintf(stderr, "Texture layout must be between 0 and %d\n",
									max);
		return -1;
	}

	_width = image.get_width();
	_height = image.get_height();
	_depth = 1;

	GLenum in_format, out_format, out_type;

	/*
	 * Set the internalFormat (in_format), format of pixel data
	 * (out_format), and the data type of the pixel data based on bits per
	 * pixel. These settings seem to be the most common.
	 */
	switch (image.get_bpp()) {
	case 8:
		in_format = GL_R3_G3_B2;
		out_format = GL_RGB;
		out_type = GL_UNSIGNED_BYTE_3_3_2;
		break;
	case 16:
		in_format = GL_RGB5;
		out_format = GL_RGB;
		out_type = GL_UNSIGNED_SHORT_5_6_5;
		break;
	case 24:
		in_format = GL_RGB;
		out_format = GL_BGR;
		out_type = GL_UNSIGNED_BYTE;
		break;
	default:
		in_format = GL_RGBA;
		out_format = GL_BGRA;
		out_type = GL_UNSIGNED_BYTE;
		break;
	}

	_offset = layout_offset;
	glActiveTexture(GL_TEXTURE0 + _offset);
	glBindTexture(GL_TEXTURE_2D, _id);

	glTexImage2D(GL_TEXTURE_2D,	  /* GLenum target */
		     0,			  /* GLint level, the mipmap level */
		     in_format,		  /* GLint internalFormat */
		     _width,		  /* GLsizei width */
		     _height,		  /* GLsizei height */
		     0,			  /* GLint border, must be 0 */
		     out_format,	  /* GLenum format */
		     out_type,		  /* GLenum type */
		     image.get_pixels()); /* const GLvoid *data */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);

	glBindTexture(GL_TEXTURE_2D, 0);

	return 0;
}

void Texture2D::bind()
{
	if (!_bound) {
		glActiveTexture(GL_TEXTURE0 + _offset);
		glBindTexture(GL_TEXTURE_2D, _id);
		_bound = 1;
	}
}

void Texture2D::unbind()
{
	if (_bound) {
		glActiveTexture(GL_TEXTURE0 + _offset);
		glBindTexture(GL_TEXTURE_2D, 0);
		_bound = 0;
	}
}

} /* namespace dengine */
