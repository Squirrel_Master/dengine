#include <graphics/renderbuf3d.h>

namespace dengine {

Renderbuf3D::Renderbuf3D() : _ibo(0),
			     _vao(0),
			     _vbo(0),
			     _buf(NULL),
			     _draw_calls(0),
			     _num_vertices(0),
			     _num_indices(0),
			     _sub_vertices(0),
			     _sub_indices(0),
			     _buf_idx(NULL)
{
}

Renderbuf3D::~Renderbuf3D()
{
	destroy();
}

void Renderbuf3D::destroy()
{
	if (_ibo) {
		glDeleteBuffers(1, &_ibo);
		_ibo = 0;
	}

	if (_vbo) {
		glDeleteBuffers(1, &_vbo);
		_vbo = 0;
	}

	if (_vao) {
		glDeleteVertexArrays(1, &_vao);
	}

	_num_vertices = 0;
	_num_indices = 0;

	/* Delete textures. */
	for (set_iter it = _tslots.begin(); it != _tslots.end(); it++)
		delete *it;
	_tslots.clear();
}

void Renderbuf3D::begin()
{
	if (!_vao || !_vbo || !_ibo) {
		fprintf(stderr, "Renderbuf3D not initialized.\n");
		return;
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ibo);
	glBindBuffer(GL_ARRAY_BUFFER, _vbo);

	_buf = (MeshVertex *)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

	_buf_idx = (GLuint *)glMapBuffer(GL_ELEMENT_ARRAY_BUFFER,
								GL_WRITE_ONLY);

	_sub_vertices = 0;
	_sub_indices = 0;
}

void Renderbuf3D::end()
{
	if (!_vao || !_vbo || !_ibo) {
		fprintf(stderr, "Renderbuf3D not initialized.\n");
		return;
	}

	glUnmapBuffer(GL_ARRAY_BUFFER);
	glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Renderbuf3D::flush()
{
	if (!_vao || !_vbo || !_ibo) {
		fprintf(stderr, "Renderbuf3D not initialized.\n");
		return;
	}

	_draw_calls++;

	for (set_iter it = _tslots.begin(); it != _tslots.end(); it++)
		(*it)->bind();

	glBindVertexArray(_vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ibo);

	glDrawElements(GL_TRIANGLES, _sub_indices, GL_UNSIGNED_INT, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	_sub_indices = 0;

	for (set_iter it = _tslots.begin(); it != _tslots.end(); it++)
		(*it)->unbind();
}

void Renderbuf3D::init()
{
	GLint vbo_max_size, ibo_max_size;
	glGetIntegerv(GL_MAX_ELEMENTS_VERTICES, &vbo_max_size);
	glGetIntegerv(GL_MAX_ELEMENTS_INDICES, &ibo_max_size);

	init((unsigned int)(vbo_max_size / MVERTEX_SIZE),
		(unsigned int)(ibo_max_size / sizeof(unsigned int)));

	/* Grow the buffers based on the sizes of the renderables. */
//	init(1, 1);
}

void Renderbuf3D::init(unsigned int vbo_size, unsigned int ibo_size)
{
	destroy();
	glGenVertexArrays(1, &_vao);

	init_vbo(vbo_size);
	init_ibo(ibo_size);
}

/*
 * init_vbo:
 * Initializes the vbo based on the number of vertices.
 */
void Renderbuf3D::init_vbo(unsigned int num_vertices)
{
	if (num_vertices <= _num_vertices)
		return;

	if (!_vao) {
		fprintf(stderr, "Renderbuf3D not initialized.\n");
		return;
	}

	_num_vertices = num_vertices;

	glBindVertexArray(_vao);

	/* Delete the current vertex buffer object and generate a new one. */
	if (_vbo)
		glDeleteBuffers(1, &_vbo);
	glGenBuffers(1, &_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, _vbo);

	/*
	 * Specify the size of the VBO, its location, and how to use it.
	 * GL_ARRAY_BUFFER essentially specifies that the VBO is part of
	 * a VAO.
	 */
	glBufferData(GL_ARRAY_BUFFER,
		     _num_vertices * MVERTEX_SIZE,
		     0,
		     GL_STATIC_DRAW);

	glEnableVertexAttribArray(MPOSITION_INDEX);
	glEnableVertexAttribArray(MNORMAL_INDEX);
	glEnableVertexAttribArray(MTANGENT_INDEX);
	glEnableVertexAttribArray(MUV_INDEX);
	glEnableVertexAttribArray(MTID_INDEX);
	glEnableVertexAttribArray(MTIDX_INDEX);
	glEnableVertexAttribArray(MCOLOR_INDEX);

	/* Position coordinate data. */
	glVertexAttribPointer(MPOSITION_INDEX,
			      3,
			      GL_FLOAT,
			      GL_FALSE,
			      MVERTEX_SIZE,
			      MPOSITION_OFFSET);

	/* Normal vector data. */
	glVertexAttribPointer(MNORMAL_INDEX,
			      3,
			      GL_FLOAT,
			      GL_FALSE,
			      MVERTEX_SIZE,
			      MNORMAL_OFFSET);

	/* Tangent vector data. */
	glVertexAttribPointer(MTANGENT_INDEX,
			      3,
			      GL_FLOAT,
			      GL_FALSE,
			      MVERTEX_SIZE,
			      MTANGENT_OFFSET);

	/* Texture coordinate data. */
	glVertexAttribPointer(MUV_INDEX,
			      2,
			      GL_FLOAT,
			      GL_FALSE,
			      MVERTEX_SIZE,
			      MUV_OFFSET);

	/* Texture Array ID (specifies which sampler2DArray to use). */
	glVertexAttribPointer(MTID_INDEX,
			      1,
			      GL_FLOAT,
			      GL_FALSE,
			      MVERTEX_SIZE,
			      MTID_OFFSET);

	/* Texture index (specifies the texture within the sampler2DArray). */
	glVertexAttribPointer(MTIDX_INDEX,
			      1,
			      GL_FLOAT,
			      GL_FALSE,
			      MVERTEX_SIZE,
			      MTIDX_OFFSET);

	/* Vertex color. */
	glVertexAttribPointer(MCOLOR_INDEX,
			      4,
			      GL_UNSIGNED_BYTE,
			      GL_TRUE,
			      MVERTEX_SIZE,
			      MCOLOR_OFFSET);

	/* Unbind the vao and the vbo. */
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void Renderbuf3D::init_ibo(unsigned int num_indices)
{
	if (num_indices <= _num_indices)
		return;

	if (!_vao) {
		fprintf(stderr, "Renderbuf3D not initialized.\n");
		return;
	}

	glBindVertexArray(_vao);

	_num_indices = num_indices;

	/* Delete current index buffer and generate a new one. */
	if (_ibo)
		glDeleteBuffers(1, &_ibo);

	glGenBuffers(1, &_ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ibo);

	/*
	 * Using GL_DYNAMIC_DRAW since indices will change as renderables are
	 * added or removed from the renderer and its associated renderbuf
	 * (this).
	 */
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		     _num_indices * sizeof(GLuint),
		     NULL,
		     GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}

void Renderbuf3D::insert_texture(Texture *t)
{
	if (t) {
		set_iter it = _tslots.find(t);
		if (it == _tslots.end())
			_tslots.insert(t);
	}
}

} /* namespace dengine */